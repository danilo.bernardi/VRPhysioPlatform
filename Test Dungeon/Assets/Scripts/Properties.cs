﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Properties : MonoBehaviour {

	private const string APP_NAME = "VRPhysioPlatform";
	private const string CONTROLLER_FILE = "controller_vr.txt";
	private const string VIDEO_FILE = "vrVideo_TEMP.mp4";
	private const string CONFIG_FILE = "config.txt";
	private const string VR_VIDEO = "vrVideo";
	private const string UNITY = "Unity";
	private const string JAVA = "Java";
	private const string USED = "used";
	private const string CREATED_BY = "createdBy";
	private const string PATH = "path";
	private const string TRUE = "true";
	private const string FALSE = "false";
	public static string currentFolder{ get; set;}
	public static string filePath{ get; set;}
	public static string controllerPath{ get; set;}
	public static long fileSize { get; set;}	 // bytes
	public static long fileDuration { get; set;} 	// milliseconds
	public static long timestamp { get; set;} 
	private string appFolder;
	private string configFilePath;

	void Start () {

		string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
		appFolder = documentsPath + "\\" + APP_NAME;
		configFilePath = appFolder + "\\" + CONFIG_FILE;   
		Debug.Log ("documentsPath: " + documentsPath);

	

		if (File.Exists (configFilePath)) {

			StreamReader reader = new StreamReader (configFilePath); 
			string properties = reader.ReadToEnd ();
			reader.Close ();

			JSONObject propertiesObject = new JSONObject (properties);

			string used = propertiesObject.GetField(USED).str;
			string createdBy = propertiesObject.GetField(CREATED_BY).str;
			string path = propertiesObject.GetField(PATH).str;
		

			Debug.Log ("used: " + used + ", FALSE: " + FALSE);
			Debug.Log ("createdBy: " + createdBy + ", JAVA: " + JAVA);
			Debug.Log ("path: " + path);

			Debug.Log ("createdBy equals: " + createdBy.Equals (JAVA));
			Debug.Log ("used equals: " + used.ToLower().Equals (FALSE));


			if (createdBy.Equals (JAVA) && used.ToLower().Equals (FALSE)) {
				Debug.Log("Folder created by Java and not used. Using it and updating config...");

				currentFolder = path;
				propertiesObject.RemoveField (USED);
				propertiesObject.AddField (USED, TRUE);
				using (StreamWriter file = new StreamWriter(configFilePath, false)){
					file.WriteLine(propertiesObject.ToString());
				}
			}

			else {
				Debug.Log("Folder created by Unity and/or already used. Creating new folder...");
				createFolder();
			}
				
		} else {
			Debug.Log ("Config not present. Creating config and folder...");
			createFolder();
		}

		controllerPath = currentFolder + "\\" + CONTROLLER_FILE;
		filePath = currentFolder + "\\" + VIDEO_FILE;
	}

	private void createFolder(){
		string now = System.DateTime.Now.ToString("yyyyMMdd_HHmmss");
		currentFolder = appFolder + "\\" + now;
		Directory.CreateDirectory(currentFolder);

		JSONObject propertiesObject = new JSONObject ();
		propertiesObject.AddField (PATH, currentFolder.Replace ("\\", "\\\\"));
//		propertiesObject.AddField (PATH, currentFolder);

		propertiesObject.AddField (CREATED_BY, UNITY);
		propertiesObject.AddField (USED, FALSE);

		using (StreamWriter file = new StreamWriter(configFilePath, false)){
			file.WriteLine(propertiesObject.ToString());
		}
			
	}

}
