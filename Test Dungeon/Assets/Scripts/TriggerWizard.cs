﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerWizard : MonoBehaviour {

	public GameObject wizard;
	public GameObject captureCamera;
	private Animator animator;
//	private GameObject wayPoint;
	private GameObject target;
	private bool trigger;
	private bool attack;
	private float speed = 10.0f;
	private float timer = 0.5f;
	private AudioSource[] audioSources;

	// Use this for initialization
	void Start () {
		animator = wizard.GetComponent<Animator> ();
//		wayPoint = GameObject.Find("wayPoint");
		target = GameObject.Find("[CameraRig]");
		wizard.SetActive (false);
		audioSources = captureCamera.GetComponents<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
//		if(timer > 0){
//			timer -= Time.deltaTime;
//		}
//		if(timer <= 0){
//			//The position of the waypoint will update to the player's position
//			wayPoint.transform.position = target.transform.position;
//			timer = 0.5f;
//		}

		if (wizard != null) {
			Vector3 position = new Vector3 (target.transform.position.x, wizard.transform.position.y, target.transform.position.z);
			wizard.transform.LookAt (position);

			if (trigger) {
//				Vector3 wayPointPos = new Vector3 (wayPoint.transform.position.x, wayPoint.transform.position.y, wayPoint.transform.position.z);
				Vector3 targetPos = new Vector3 (target.transform.position.x, target.transform.position.y+0.5f, target.transform.position.z);
				wizard.transform.position = Vector3.MoveTowards (wizard.transform.position, targetPos, speed * Time.deltaTime);
			}
			float distance = Vector3.Distance (wizard.transform.position, target.transform.position);
			if (!attack) {
				if (distance < 10) {
					animator.SetBool ("attack", true);
					attack = true;
				}
			}
			if (distance < 0.6f)
				Destroy (wizard);
		}
	}

	void OnTriggerEnter (Collider other){
		if (wizard != null) {
			wizard.SetActive (true);
			trigger = true;
			audioSources [1].PlayOneShot (audioSources[1].clip);
//			if (audioSources [0].isPlaying) {
//				audioSources [0].Pause ();
//				audioSources [1].PlayOneShot (audioSources[1].clip);
//			}

		}
	}
}
