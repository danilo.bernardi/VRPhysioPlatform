﻿using System;

namespace AssemblyCSharp{

	[Serializable]
	public class VRVideoMetadata{

		public long timestamp;
		public int frameRate;
		public long duration;
		public long size;
		public string path;

		public VRVideoMetadata (long timestamp, int frameRate, long duration, long size, string path){
			this.timestamp = timestamp;
			this.frameRate = frameRate;
			this.duration = duration;
			this.size = size;
			this.path = path;
		}
			
		public override string ToString (){
			return string.Format ("[VRVideoMetadata: timestamp={0}, frameRate={1}, duration={2}, size={3}, path={4}]", timestamp, frameRate, duration, size, path);
		}
		
	}
}

