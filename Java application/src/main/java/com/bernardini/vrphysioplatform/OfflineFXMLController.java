
package com.bernardini.vrphysioplatform;

import com.bernardini.vrphysioplatform.domain.Marker;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.swing.filechooser.FileSystemView;
//import plux.newdriver.bioplux.BPException;
//import plux.newdriver.bioplux.Device;

/**
 *
 * @author danilo.bernardi
 */
public class OfflineFXMLController implements Initializable {

    private static final Logger logger = Logger.getLogger(OfflineFXMLController.class.getName());
    private static final String APP_NAME = "VRPhysioPlatform";
    private static final String DEVICE = "00:07:80:FC:57:19";
    private static final String VR_VIDEO = "vrVideo.mp4";
    private static final String CAMERA_VIDEO = "cameraVideo.mp4";
    private static final String SIGNALS = "signals.txt";
    private static final String CONTROLLER_FILE = "controller.txt";
    private static final String PLAY = "Play";
    private static final String PAUSE = "Pause";
    private static final String COLUMNS = "columns";
    private static final String MARKERS = "markers";
    private static final String TIMESTAMP = "timestamp";
    private static final String NAME = "name";
    private static final String SAMPLING_RATE = "samplingRate";
    private static final String WIDTH = "width";
    private static final String HEIGHT = "height";
    private static final double MIN_CHANGE = 0.1;
    private static final double SLIDER_MARGIN_RL = 5;
    private static final double SLIDER_MARGIN_BOTTOM = 2;
    private static final double A0 = 1.12764514e-3;
    private static final double A1 = 2.34282709e-4;
    private static final double A2 = 8.77303013e-8;
    private static final int N_BITS = 16;
    private static final int VCC = 3;
    private static final double KELVIN_CELSIUS = 273.15;
    private static final double C_MIN = 27400;
    private static final double C_MAX = 39100;
    private static final double EMG_ECG_GAIN = 1000;
    private static final double EEG_GAIN = 40000;
    private static final double EDA_CONST = 0.12;
    private static final double POW_2_N = Math.pow(2, N_BITS);
    

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private MediaView vrMediaView, cameraMediaView;
    
    @FXML
    private Slider vrSlider, cameraSlider, slider;
    
    @FXML
    private Label timeLabel;
    
    @FXML
    private Button playButton, stopButton, insertMarkerButton;
    
    @FXML
    private MenuItem offlineItem, realtimeItem;
    
    @FXML
    private LineChart<String, Number> ecgChart, emgChart, forceChart, tempChart,
            edaChart, respChart, eegChart, accChart;
    
    private XYChart.Series<String, Number> seriesX, seriesY, seriesZ, seriesTemp,
            seriesEda, seriesEcg, seriesEmg, seriesForce, seriesResp, seriesEeg;

    private String currentFolder;
    private String appFolder;
    private String controllerPath;
    private MediaPlayer vrMediaPlayer;
    private MediaPlayer cameraMediaPlayer;
    private String videoPath1;
    private String videoPath2;
    private String signalsPath;
    private boolean play = false;
    private boolean stop = false;
    private int signalsCounter = 0;
    private BufferedReader br;
    private int samplingRate;
    private double signalsDuration;
    private double stageWidth;
    private double stageHeigth;
    private int markerRadius;
    private Scene scene;
    private Stage stage;
    private Gson gson;
    
    private ArrayList<String> signals = new ArrayList<>();

    private int accXPos,accYPos,accZPos,ecgPos,edaPos,eegPos,emgPos,forcePos,respPos,tempPos;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        gson = new Gson();
        String documentsPath = FileSystemView.getFileSystemView().getDefaultDirectory().getPath();
        appFolder = documentsPath + "\\" + APP_NAME;    

        seriesX = new XYChart.Series<>();
        seriesX.setName("X");
        seriesY = new XYChart.Series<>();
        seriesY.setName("Y");
        seriesZ = new XYChart.Series<>();
        seriesZ.setName("Z");
        seriesTemp = new XYChart.Series<>();
        seriesTemp.setName("Temp");
        seriesEcg = new XYChart.Series<>();
        seriesEcg.setName("ECG");
        seriesEda = new XYChart.Series<>();
        seriesEda.setName("EDA");
        seriesEeg = new XYChart.Series<>();
        seriesEeg.setName("EEG");
        seriesEmg = new XYChart.Series<>();
        seriesEmg.setName("EMG");
        seriesForce = new XYChart.Series<>();
        seriesForce.setName("Force");
        seriesResp = new XYChart.Series<>();
        seriesResp.setName("Resp");
        
        for (int i = 0; i < 10; i++){
            seriesX.getData().add(new XYChart.Data<>(i + "", 0));
            seriesY.getData().add(new XYChart.Data<>(i + "", 0));
            seriesZ.getData().add(new XYChart.Data<>(i + "", 0));
            seriesTemp.getData().add(new XYChart.Data<>(i + "", 0));
            seriesEcg.getData().add(new XYChart.Data<>(i + "", 0));
            seriesEda.getData().add(new XYChart.Data<>(i + "", 0));
            seriesEeg.getData().add(new XYChart.Data<>(i + "", 0));
            seriesEmg.getData().add(new XYChart.Data<>(i + "", 0));
            seriesForce.getData().add(new XYChart.Data<>(i + "", 0));
            seriesResp.getData().add(new XYChart.Data<>(i + "", 0));
        }
        
        tempChart.setCreateSymbols(false);
        ecgChart.setCreateSymbols(false);
        emgChart.setCreateSymbols(false);
        forceChart.setCreateSymbols(false);            
        edaChart.setCreateSymbols(false);
        respChart.setCreateSymbols(false);
        eegChart.setCreateSymbols(false);
        accChart.setCreateSymbols(false);
        
        playButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (play){
                    pause();
                }
                else {
                    play();
                }
            }
        });
        
        stopButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stop();
            }
        });
    }
    
    @FXML
    private void open(ActionEvent event){
        scene = anchorPane.getScene();
        stage = (Stage) scene.getWindow();
        
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("JavaFX Projects");
        File defaultDirectory = new File(appFolder);
        chooser.setInitialDirectory(defaultDirectory);
        File selectedDirectory = chooser.showDialog(stage);
        if (selectedDirectory != null){
            currentFolder = selectedDirectory.getAbsolutePath();
            String vrVideoPath = currentFolder + "\\" + VR_VIDEO;
            String cameraVideoPath = currentFolder + "\\" + CAMERA_VIDEO;
            String signalsPath = currentFolder + "\\" + SIGNALS;
            controllerPath = currentFolder + "\\" + CONTROLLER_FILE;
            
            openVrVideo(vrVideoPath);
            openCameraVideo(cameraVideoPath);
            openSignals(signalsPath);

            playButton.setDisable(false);
            stopButton.setDisable(false);
            insertMarkerButton.setDisable(false);
            
            
        }
    }

    private void openVrVideo(String path) {
        if (path != null) {
            File file = new File(path);
            Media media = new Media(file.toURI().toString());
            vrMediaPlayer = new MediaPlayer(media);
            vrMediaView.setMediaPlayer(vrMediaPlayer);
            
            vrMediaPlayer.totalDurationProperty().addListener((obs, oldDuration, newDuration) -> vrSlider.setMax(newDuration.toSeconds()));
            
            vrSlider.valueChangingProperty().addListener((obs, wasChanging, isChanging) -> {
                if (! isChanging) {
                    vrMediaPlayer.seek(Duration.seconds(vrSlider.getValue()));
                }
            });
            
            vrSlider.valueProperty().addListener((obs, oldValue, newValue) -> {
                if (! vrSlider.isValueChanging()) {
                    double currentTime = vrMediaPlayer.getCurrentTime().toSeconds();
                    if (Math.abs(currentTime - newValue.doubleValue()) > MIN_CHANGE) {
                        vrMediaPlayer.seek(Duration.seconds(newValue.doubleValue()));
                    }
                }
            });

            vrMediaPlayer.currentTimeProperty().addListener((obs, oldTime, newTime) -> {
                if (! vrSlider.isValueChanging()) {
                    vrSlider.setValue(newTime.toSeconds());
                }
            });
            
            vrMediaPlayer.setOnEndOfMedia(new Runnable() {
                @Override
                public void run() {
                    stop();
                }
            });
            
           
            timeLabel.textProperty().bind(Bindings.createStringBinding(() -> {
                        Duration time = vrMediaPlayer.getCurrentTime();
                        print("TIME: " + time.toSeconds());
                        print("TIME to seconds: " + time.toSeconds() % 3600);
                        return String.format("%4d:%02d:%04.1f",
                            (int) time.toHours(),
                            (int) time.toMinutes() % 60,
                            time.toSeconds() % 3600);
                    },
                    vrMediaPlayer.currentTimeProperty()));
            
            
            markerRadius = (int)(slider.getHeight()/2);
        

            DoubleProperty width = vrMediaView.fitWidthProperty();
            DoubleProperty height = vrMediaView.fitHeightProperty();
            
            width.bind(Bindings.selectDouble(vrMediaView.parentProperty(), WIDTH));
            height.bind(Bindings.selectDouble(vrMediaView.parentProperty(), HEIGHT));

            scene = anchorPane.getScene();
            stage = (Stage) scene.getWindow();
            stageWidth = stage.getWidth();
            stageHeigth = stage.getHeight();
        }
    }

    private void openCameraVideo(String path) {
        if (path != null) {
            File file = new File(path);
            Media media = new Media(file.toURI().toString());
            cameraMediaPlayer = new MediaPlayer(media);
            cameraMediaView.setMediaPlayer(cameraMediaPlayer);
      
            cameraMediaPlayer.totalDurationProperty().addListener((obs, oldDuration, newDuration) -> 
                    cameraSlider.setMax(newDuration.toSeconds()));
            
            cameraSlider.valueChangingProperty().addListener((obs, wasChanging, isChanging) -> {
                if (! isChanging) {
                    cameraMediaPlayer.seek(Duration.seconds(cameraSlider.getValue()));
                }
            });
            
            cameraSlider.valueProperty().addListener((obs, oldValue, newValue) -> {
                if (! cameraSlider.isValueChanging()) {
                    double currentTime = cameraMediaPlayer.getCurrentTime().toSeconds();
                    if (Math.abs(currentTime - newValue.doubleValue()) > MIN_CHANGE) {
                        cameraMediaPlayer.seek(Duration.seconds(newValue.doubleValue()));
                    }
                }
            });

            cameraMediaPlayer.currentTimeProperty().addListener((obs, oldTime, newTime) -> {
                if (! cameraSlider.isValueChanging()) {
                    cameraSlider.setValue(newTime.toSeconds());
                }
            });
           

            DoubleProperty width = cameraMediaView.fitWidthProperty();
            DoubleProperty height = cameraMediaView.fitHeightProperty();
            width.bind(Bindings.selectDouble(vrMediaView.parentProperty(), WIDTH));
            height.bind(Bindings.selectDouble(vrMediaView.parentProperty(), HEIGHT));

        }
    }

    private void playVrVideo() {
        if (vrMediaPlayer != null)
            vrMediaPlayer.play();
    }

    private void pauseVrVideo() {
        if (vrMediaPlayer != null)
            vrMediaPlayer.pause();
    }

    private void stopVrVideo() {
        if (vrMediaPlayer != null)
            vrMediaPlayer.stop();
    }

    private void playCameraVideo() {
        if (cameraMediaPlayer != null)
            cameraMediaPlayer.play();
    }

    private void pauseCameraVideo() {
        if (cameraMediaPlayer != null)
            cameraMediaPlayer.pause();
    }

    private void stopCameraVideo() {
        if (cameraMediaPlayer != null)
            cameraMediaPlayer.stop();
    }
    
    private void openSignals(String path) {
        try {
            path = path.replace("\\", "\\\\");
            br = new BufferedReader(new FileReader(path));
            String properties = br.readLine().substring(2);
            
            JsonObject propertiesObject = gson.fromJson(properties, JsonObject.class);
            
            JsonArray columns = propertiesObject.get(COLUMNS).getAsJsonArray();
            samplingRate = propertiesObject.get(SAMPLING_RATE).getAsInt();

            for (int i = 0; i < columns.size(); i++){
                JsonElement columnElement = columns.get(i); 
                if (!columnElement.isJsonNull()){
                    String column = columnElement.getAsString();
                    switch(column){
                        case "ACCX":
                            accChart.getData().add(seriesX);
                            accXPos = i;
                            break;
                        case "ACCY":
                            accChart.getData().add(seriesY);
                            accYPos = i;
                            break;
                        case "ACCZ":
                            accChart.getData().add(seriesZ);
                            accZPos = i;
                            break;
                        case "ECG":
                            ecgChart.getData().add(seriesEcg);
                            ecgPos = i;
                            break;
                        case "EDA":
                            edaChart.getData().add(seriesEda);
                            edaPos = i;
                            break;
                        case "EEG":
                            eegChart.getData().add(seriesEeg);
                            eegPos = i;
                            break;
                        case "EMG":
                            emgChart.getData().add(seriesEmg);
                            emgPos = i;
                            break;
                        case "Force":
                            forceChart.getData().add(seriesForce);
                            forcePos = i;
                            break;
                        case "Resp":
                            respChart.getData().add(seriesResp);
                            respPos = i;
                            break;
                        case "Temp":
                            tempChart.getData().add(seriesTemp);
                            tempPos = i;
                            break;
                    }

                }      
            }
            
            // populate SIGNALS with sensors data
            String line;
            while ((line = br.readLine()) != null) {
                signals.add(line);
            }

            
            vrMediaPlayer.setOnReady(new Runnable() {
                @Override
                public void run() {
                       
                    double duration = vrMediaPlayer.totalDurationProperty().getValue().toSeconds();

                    slider.setMax(duration);
                    slider.setValue(0);
                    slider.setBlockIncrement(0.1);
                    
                    addMarkers();


                    vrSlider.valueProperty().addListener((obs, oldValue, newValue) -> {
                        if (! vrSlider.isValueChanging()) {
                            slider.setValue(vrSlider.getValue());
                        }
                    });

                    cameraSlider.valueProperty().addListener((obs, oldValue, newValue) -> {
                        if (! cameraSlider.isValueChanging()) {
                            slider.setValue(cameraSlider.getValue());
                        }
                    });
                }
            });

            slider.valueProperty().addListener((obs, oldValue, newValue) -> {
                    if (! slider.isValueChanging()) {
                        double value = slider.getValue();
                        vrSlider.setValue(value);
                        cameraSlider.setValue(value);
                        signalsCounter = (int)(value*1000);
                    }
                });
            

            Timeline updateGraphs = new Timeline(new KeyFrame(Duration.millis(10), (ActionEvent event) -> {
                if (play){
                    try {
                        if (seriesTemp.getData().size() >= 100){
                            seriesX.getData().remove(0);
                            seriesY.getData().remove(0);
                            seriesZ.getData().remove(0);
                            seriesTemp.getData().remove(0);
                            seriesEcg.getData().remove(0);
                            seriesEda.getData().remove(0);
                            seriesEeg.getData().remove(0);
                            seriesEmg.getData().remove(0);
                            seriesResp.getData().remove(0);
                            seriesForce.getData().remove(0);
                        }

                        String data = signals.get(signalsCounter);
                        print(data);

                        String[] parts = data.split("\t");

                        double accX = transAcc(Integer.parseInt(parts[accXPos]));
                        double accY = transAcc(Integer.parseInt(parts[accYPos]));
                        double accZ = transAcc(Integer.parseInt(parts[accZPos]));
                        double ecg = transEcg(Integer.parseInt(parts[ecgPos]));
                        double eeg = transEeg(Integer.parseInt(parts[eegPos]));
                        double emg = transEmg(Integer.parseInt(parts[emgPos]));
                        double eda = transEda(Integer.parseInt(parts[edaPos]));
                        double force = transForce(Integer.parseInt(parts[forcePos]));
                        double resp = transResp(Integer.parseInt(parts[respPos]));
                        double temp = round(transTemp(Integer.parseInt(parts[tempPos])),1);

                        seriesX.getData().add(new XYChart.Data<>(signalsCounter + "", accX));
                        seriesY.getData().add(new XYChart.Data<>(signalsCounter + "", accY));
                        seriesZ.getData().add(new XYChart.Data<>(signalsCounter + "", accZ));
                        seriesEcg.getData().add(new XYChart.Data<>(signalsCounter + "", ecg));
                        seriesTemp.getData().add(new XYChart.Data<>(signalsCounter + "", temp));
                        seriesResp.getData().add(new XYChart.Data<>(signalsCounter + "", resp));
                        seriesForce.getData().add(new XYChart.Data<>(signalsCounter + "", force));
                        seriesEmg.getData().add(new XYChart.Data<>(signalsCounter + "", emg));
                        seriesEda.getData().add(new XYChart.Data<>(signalsCounter + "", eda));
                        seriesEeg.getData().add(new XYChart.Data<>(signalsCounter + "", eeg));

//                        signalsCounter++;
                    } catch (Exception ex) {
                        logger.log(Level.SEVERE, null, ex);
                    }
                }
            }));
        updateGraphs.setCycleCount(Timeline.INDEFINITE);
        updateGraphs.play();
            
        }
        catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }
    
    private void playSignals() {
        play = true;
    }

    private void pauseSignals() {
        play = false;
    }

    private void stopSignals() {
        play = false;
        stop = true;
        signalsCounter = 0;
//        sliderSignals.setValue(0);
    }
    
    private void play() {
        playVrVideo();
        playCameraVideo();
        playSignals();
        playButton.setText(PAUSE);
    }

    private void pause() {
        pauseVrVideo();
        pauseCameraVideo();
        pauseSignals();
        playButton.setText(PLAY);
    }

    private void stop() {
        stopVrVideo();
        stopCameraVideo();
        stopSignals();
        playButton.setText(PLAY);
    }
    
    private double transTemp(int rawData){
        double ntcV = rawData*VCC/POW_2_N;
        double ntcO = 10000*ntcV/(VCC-ntcV);
        double logO = Math.log(ntcO);
        double tempK = 1/(A0+(A1*logO)+(A2*Math.pow(logO,3)));
        double tempC = tempK-KELVIN_CELSIUS;      
        return tempC;
    }
    
    private double transAcc(int rawData){
        double acc = (2*(rawData-C_MIN)/(C_MAX-C_MIN))-1;
        return acc;
    }
    
    private double transResp(int rawData){
        double perc = ((rawData/POW_2_N)-0.5)*100;
        return perc;
    }
    
    private double transEmg(int rawData){
        double emgV = (((rawData/POW_2_N)-0.5)*VCC)/EMG_ECG_GAIN;
        double emgMV = emgV*1000;
        return emgMV;
    }
    
    private double transEda(int rawData){
        double edaMS = (rawData/POW_2_N*VCC)/EDA_CONST;
        return edaMS;
    }
    
    private double transEcg(int rawData){
        double ecgV = (((rawData/POW_2_N)-0.5)*VCC)/EMG_ECG_GAIN;
        double ecgMV = ecgV*1000;
        return ecgMV;
    }
    
    private double transEeg(int rawData){
        double eegV = (((rawData/POW_2_N)-0.5)*VCC)/EEG_GAIN;
        double eegMV = eegV*1000000;
        return eegMV;
    }
    
    private double transForce(int rawData){
        double force = rawData*100/POW_2_N;
        return force;
    }
    
    private double round(double value, int places) {
        if (places < 0) 
            throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    private void print(String s){
        System.out.println(s);
    }
    
    @FXML
    private void insertMarker(ActionEvent event) throws IOException {
        
        pause();
        File file = new File(controllerPath);
        if (!file.exists()){
            FileWriter writer = new FileWriter(controllerPath);
            JsonObject markersObject = new JsonObject();
            markersObject.add(MARKERS, new JsonArray());
            String markersString = gson.toJson(markersObject);
            writer.write(markersString);
            writer.close();
        }
        
        int timestamp = (int)(slider.getValue()*1000);
        print("INSERT - slider.getWidth(): " + slider.getWidth());
        print("INSERT - slider.getMax(): " + slider.getMax());
        print("INSERT - slider.getWidth()/slider.getMax(): " + slider.getWidth()/slider.getMax());
        print("INSERT - Marker timestamp: " + timestamp);
        
//        log.log(Level.INFO, "sliderWidth: {0}", slider1.getWidth());
//        log.log(Level.INFO, "sliderValue: {0}", slider1.getValue());
//        log.log(Level.INFO, "sliderMax: {0}", slider1.getMax());
        
        double offset = slider.getValue()*slider.getWidth()/slider.getMax();
        print("INSERT - Marker offset: " + offset);
//        double offset1 = slider1.getValue()*slider1.getWidth()/slider1.getMax();
//        double offset2 = slider2.getValue()*slider2.getWidth()/slider2.getMax();
//        double offsetSignals = sliderSignals.getValue()*sliderSignals.getWidth()/sliderSignals.getMax();
        
        Marker marker = new Marker(timestamp, 8, slider.getHeight(), offset);

        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Event name");
        dialog.setHeaderText("Insert event marker");
        dialog.setContentText("Name of the event:");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(name -> marker.setName(name));

        
        JsonObject markerObject = new JsonObject();
        markerObject.addProperty(TIMESTAMP, timestamp);
        markerObject.addProperty(NAME, marker.getName());
        
        String markersString = readFile(controllerPath);
        JsonObject markersObject = gson.fromJson(markersString, JsonObject.class);
        markersObject.get(MARKERS).getAsJsonArray().add(markerObject);
        String newMarkersString = gson.toJson(markersObject);
        FileWriter writer = new FileWriter(controllerPath);
        writer.write(newMarkersString);
        writer.close();    
        
        drawMarker(marker, offset);

    }
    
    private void addMarkers(){
        String markersString = readFile(controllerPath);
        if (markersString != null) {
            JsonObject markersObject = gson.fromJson(markersString, JsonObject.class);
            JsonArray markersArray = markersObject.get(MARKERS).getAsJsonArray();
            for (int i = 0; i < markersArray.size(); i++) {
                JsonObject markerObject = markersArray.get(i).getAsJsonObject();
                String name = "";
                if (markerObject.has(NAME)) {
                    name = markerObject.get(NAME).getAsString();
                }
                int timestamp = markerObject.get(TIMESTAMP).getAsInt();
                double offset = timestamp / 1000 * slider.getWidth() / slider.getMax();

                Marker marker = new Marker(name, timestamp, 8, slider.getHeight(), offset);

                drawMarker(marker, offset);
            }
        }
    }
    
    private void drawMarker(Marker marker, double offset){
        Tooltip tooltip = new Tooltip(marker.getName());
        Tooltip.install(marker, tooltip);
        marker.setFill(Color.RED);
        
        marker.setOnMouseClicked((MouseEvent event) -> {
            double value = marker.getOffset()*slider.getMax()/slider.getWidth();
            slider.setValue(value);
        });
        
        marker.setOnMouseEntered((MouseEvent event) -> {
            scene.setCursor(Cursor.HAND);
        });
        
        marker.setOnMouseExited((MouseEvent event) -> {
            scene.setCursor(Cursor.DEFAULT);
        });
        
        AnchorPane.setBottomAnchor(marker, 5.0);
        AnchorPane.setLeftAnchor(marker, offset);
        anchorPane.getChildren().add(marker);
    }
  
    private String readFile(String filePath){
        String result = null;
        try {
            File file = new File(filePath);
            if (file.exists()){
                FileInputStream fis = new FileInputStream(file);
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                fis.close();
                result = new String(data, "UTF-8");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
