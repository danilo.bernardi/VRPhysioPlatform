
package com.bernardini.vrphysioplatform;

import com.bernardini.vrphysioplatform.domain.CameraVideoMetadata;
import com.bernardini.vrphysioplatform.domain.SignalsConfiguration;
import com.bernardini.vrphysioplatform.domain.SignalsMetadata;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.swing.filechooser.FileSystemView;
import org.bytedeco.javacv.*;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import plux.newdriver.bioplux.*;
import org.bytedeco.javacpp.avcodec;
import org.mp4parser.Box;
import org.mp4parser.IsoFile;
import org.mp4parser.boxes.iso14496.part12.MovieBox;
import javax.sound.sampled.TargetDataLine;

/**
 *
 * @author danilo.bernardi
 */
public class RealtimeFXMLController implements Initializable {

    @FXML
    private ImageView cameraView;

    @FXML
    private Button recordButton, synchronizeButton, prepareVRButton;
    
    @FXML
    private Label syncLabel;
    
    @FXML
    private ProgressBar progressBar;
    
    @FXML
    private ComboBox<String> comboBox1, comboBox2, comboBox3, comboBox4, 
            comboBox5, comboBox6, comboBox7, comboBox8; 
    
    @FXML
    private LineChart<String, Number> ecgChart, emgChart, forceChart, tempChart,
            edaChart, respChart, eegChart, accChart;
    
    private XYChart.Series<String, Number> seriesX, seriesY, seriesZ, seriesTemp,
            seriesEda, seriesEcg, seriesEmg, seriesForce, seriesResp, seriesEeg;

    private static final Logger logger = Logger.getLogger(RealtimeFXMLController.class.getName());
    
    private static final String APP_NAME = "VRPhysioPlatform";
    private static final String CONTROLLER_VR_FILE = "controller_vr.txt";
    private static final String CONTROLLER_CAMSIG_FILE = "controller_camsig.txt";
    private static final String SIGNALS_FILE = "signals.txt";
    private static final String SIGNALS_FILE_TEMP = "signals_TEMP.txt";
    private static final String CONFIG_FILE = "config.txt";
    private static final String CAMERA_VIDEO = "cameraVideo";
    private static final String TIMESTAMP = "timestamp";
    private static final String DURATION = "duration";
    private static final String VR_VIDEO = "vrVideo";
    private static final String SIGNALS = "signals";
    private static final String CAMERA_VIDEO_NAME = "cameraVideo.mp4";
    private static final String CAMERA_VIDEO_NAME_TEMP = "cameraVideo_TEMP.mp4";
    private static final String VR_VIDEO_NAME = "vrVideo.mp4";
    private static final String VR_VIDEO_NAME_TEMP = "vrVideo_TEMP.mp4";
    private static final String PATH = "path";
    private static final String CREATED_BY = "createdBy";
    private static final String USED = "used";
    private static final String UNITY = "Unity";
    private static final String JAVA = "Java";
    private static final String MP4 = "mp4";
    private static final String TRUE = "true";
    private static final String FALSE = "false";
    private static final String FFMPEG_EXE = "ffmpeg\\bin\\ffmpeg.exe";
    private static final int SAMPLING_RATE = 1000;
    private static final int FRAME_RATE = 30;
    private static final int GOP_LENGTH_IN_FRAMES = 60;
    private static final int captureWidth = 1080;
    private static final int captureHeight = 720;
    private static final int CAMERA_ID = 1; // If Vive is active its camera is 0 and webcam is 1 
    private static final int N_BITS = 16;
    private static final double A0 = 1.12764514e-3;
    private static final double A1 = 2.34282709e-4;
    private static final double A2 = 8.77303013e-8;
    private static final int VCC = 3;
    private static final double KELVIN_CELSIUS = 273.15;
    private static final double C_MIN = 27400;
    private static final double C_MAX = 39100;
    private static final double EMG_ECG_GAIN = 1000;
    private static final double EEG_GAIN = 40000;
    private static final double EDA_CONST = 0.12;
    private static final double POW_2_N = Math.pow(2, N_BITS);
    private static final ObservableList<String> SENSORS = FXCollections.observableArrayList(
            "-", "ACC X","ACC Y","ACC Z","ECG","EDA","EEG","EMG","Force","Resp","Temp");
    
    private FileHandler fh;
    private File configFile;
    private String appFolder;
    private String currentFolder;
    private String configFilePath;
    private String controllerVrPath;
    private String controllerCamSigPath;
    private String signalsPath;
    private String signalsPathTemp;
    private String cameraVideoPath;
    private String cameraVideoPathTemp;
    private String vrVideoPath;
    private String vrVideoPathTemp;
    private String storedJsonString;
    private FileWriter signalsWriter;
    private Timestamp timestamp;
    private String ffmpeg;
    private int signalsCounter = 10;
    private Stage stage;
    private String deviceMAC;
    private Device dev;
    private Device.Frame[] frames;
    private int[] counter;
    private OpenCVFrameGrabber grabber;
    private Thread vrVideoCut;
    private Thread cameraVideoCut;
    private Java2DFrameConverter converter;
    private ScheduledExecutorService cameraTimer;
    private ScheduledExecutorService signalsTimer;
    private Timeline updateGraphs;
    private String[] columns;
    private FFmpegFrameRecorder recorder;
    private boolean record = false;
    private boolean cutCameraCompleted = false;
    private boolean cutVrCompleted = false;
    private static long startTime = 0;
    private static long videoTS = 0;
    private int accXPos,accYPos,accZPos,ecgPos,edaPos,eegPos,emgPos,forcePos,respPos,tempPos;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
                
        
        String documentsPath = FileSystemView.getFileSystemView().getDefaultDirectory().getPath();
        appFolder = documentsPath + "\\" + APP_NAME;
        configFilePath = appFolder + "\\" + CONFIG_FILE;   

        configFile = new File(configFilePath);
        try {
            if (configFile.exists()){

                String properties = readFile(configFilePath);

                Gson gson = new Gson();
                JsonObject propertiesObject = gson.fromJson(properties, JsonObject.class);

                String path = propertiesObject.get(PATH).getAsString();
                String createdBy = propertiesObject.get(CREATED_BY).getAsString();
                String used = propertiesObject.get(USED).getAsString();

                if (createdBy.equals(UNITY) && used.equals(FALSE)){
                    System.out.println("Folder created by Unity and not used. Using it and updating config...");
                    currentFolder = path;
                    propertiesObject.addProperty(USED, TRUE);
                    String propertiesString = gson.toJson(propertiesObject);
                    FileWriter writer = new FileWriter(configFile);
                    writer.write(propertiesString);
                    writer.close();
                }
                else {
                    System.out.println("Folder created by Java and/or already used. Creating new folder...");
                    createFolder();
                }

            }
            else {
                System.out.println("Config not present. Creating config and folder...");
                createFolder();
            }
        } catch (IOException ex) { 
            logger.log(Level.SEVERE, null, ex);
        }

        controllerVrPath = currentFolder + "\\" + CONTROLLER_VR_FILE;
        controllerCamSigPath = currentFolder +  "\\" + CONTROLLER_CAMSIG_FILE;
        signalsPath = currentFolder + "\\" + SIGNALS_FILE;
        signalsPathTemp = currentFolder + "\\" + SIGNALS_FILE_TEMP;

        cameraVideoPath = currentFolder + "\\" + CAMERA_VIDEO_NAME;
        cameraVideoPathTemp = currentFolder + "\\" + CAMERA_VIDEO_NAME_TEMP;

        vrVideoPath = currentFolder + "\\" + VR_VIDEO_NAME;
        vrVideoPathTemp = currentFolder + "\\" + VR_VIDEO_NAME_TEMP;
        
        Path currentRelativePath = Paths.get("");
        String workingDirectory = currentRelativePath.toAbsolutePath().toString();
        
        
        try {  
            fh = new FileHandler(workingDirectory + "\\log.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);  
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        ffmpeg = workingDirectory + "\\" + FFMPEG_EXE;
        storedJsonString = readFile(controllerVrPath);


        seriesX = new XYChart.Series<>();
        seriesX.setName("X");
        seriesY = new XYChart.Series<>();
        seriesY.setName("Y");
        seriesZ = new XYChart.Series<>();
        seriesZ.setName("Z");
        seriesTemp = new XYChart.Series<>();
        seriesTemp.setName("Temp");
        seriesEcg = new XYChart.Series<>();
        seriesEcg.setName("ECG");
        seriesEda = new XYChart.Series<>();
        seriesEda.setName("EDA");
        seriesEeg = new XYChart.Series<>();
        seriesEeg.setName("EEG");
        seriesEmg = new XYChart.Series<>();
        seriesEmg.setName("EMG");
        seriesForce = new XYChart.Series<>();
        seriesForce.setName("Force");
        seriesResp = new XYChart.Series<>();
        seriesResp.setName("Resp");

        for (int i = 0; i < 10; i++){
            seriesX.getData().add(new XYChart.Data<>(i + "", 0));
            seriesY.getData().add(new XYChart.Data<>(i + "", 0));
            seriesZ.getData().add(new XYChart.Data<>(i + "", 0));
            seriesTemp.getData().add(new XYChart.Data<>(i + "", 0));
            seriesEcg.getData().add(new XYChart.Data<>(i + "", 0));
            seriesEda.getData().add(new XYChart.Data<>(i + "", 0));
            seriesEeg.getData().add(new XYChart.Data<>(i + "", 0));
            seriesEmg.getData().add(new XYChart.Data<>(i + "", 0));
            seriesForce.getData().add(new XYChart.Data<>(i + "", 0));
            seriesResp.getData().add(new XYChart.Data<>(i + "", 0));
        }

        comboBox1.setItems(SENSORS);
        comboBox2.setItems(SENSORS);
        comboBox3.setItems(SENSORS);
        comboBox4.setItems(SENSORS);
        comboBox5.setItems(SENSORS);
        comboBox6.setItems(SENSORS);
        comboBox7.setItems(SENSORS);
        comboBox8.setItems(SENSORS);

        tempChart.setCreateSymbols(false);
        ecgChart.setCreateSymbols(false);
        emgChart.setCreateSymbols(false);
        forceChart.setCreateSymbols(false);            
        edaChart.setCreateSymbols(false);
        respChart.setCreateSymbols(false);
        eegChart.setCreateSymbols(false);
        accChart.setCreateSymbols(false);

        columns = new String[10];
        columns[0] = "seqNumber";
        columns[1] = "digitalInput";
//        synchronize("C:\\Users\\danilo.bernardi\\Documents\\VRPhysioPlatform\\20180511_171124 (Jacky)");
        comboBox1.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(oldValue != null){
                    switch (oldValue){
                        case "ACC X":
                            accChart.getData().remove(seriesX);
                            break;
                        case "ACC Y":
                            accChart.getData().remove(seriesY);
                            break;
                        case "ACC Z":
                            accChart.getData().remove(seriesZ);
                            break;
                        case "ECG":
                            ecgChart.getData().remove(seriesEcg);
                            break;
                        case "EDA":
                            edaChart.getData().remove(seriesEda);
                            break;
                        case "EEG":
                            eegChart.getData().remove(seriesEeg);
                            break;
                        case "EMG":
                            emgChart.getData().remove(seriesEmg);
                            break;
                        case "Force":
                            forceChart.getData().remove(seriesForce);
                            break;
                        case "Resp":
                            respChart.getData().remove(seriesResp);
                            break;
                        case "Temp":
                            tempChart.getData().remove(seriesTemp);
                            break;
                    }
                }
                if (newValue != null){
                    switch (newValue){
                        case "-":
                            comboBox1.getSelectionModel().clearSelection();   
                            break;
                        case "ACC X":
                            if (!accChart.getData().contains(seriesX)){
                                accChart.getData().add(seriesX);
                                accXPos = 0;
                                columns[2] = "ACCX";
                            }
                            break;
                        case "ACC Y":
                            if (!accChart.getData().contains(seriesY)){
                                accChart.getData().add(seriesY);
                                accYPos = 0;
                                columns[2] = "ACCY";
                            }
                            break;
                        case "ACC Z":
                            if (!accChart.getData().contains(seriesZ)){
                                accChart.getData().add(seriesZ);
                                accZPos = 0;
                                columns[2] = "ACCZ";
                            }
                            break;
                        case "ECG":
                            if (!ecgChart.getData().contains(seriesEcg)){
                                ecgChart.getData().add(seriesEcg);
                                ecgPos = 0;
                                columns[2] = "ECG";
                            }
                            break;
                        case "EDA":
                            if (!edaChart.getData().contains(seriesEda)){
                                edaChart.getData().add(seriesEda);
                                edaPos = 0;
                                columns[2] = "EDA";
                            }
                            break;
                        case "EEG":
                            if (!eegChart.getData().contains(seriesEeg)){
                                eegChart.getData().add(seriesEeg);
                                eegPos = 0;
                                columns[2] = "EEG";
                            }
                            break;
                        case "EMG":
                            if (!emgChart.getData().contains(seriesEmg)){
                                emgChart.getData().add(seriesEmg);
                                emgPos = 0;
                                columns[2] = "EMG";
                            }
                            break;
                        case "Force":
                            if (!forceChart.getData().contains(seriesForce)){
                                forceChart.getData().add(seriesForce);
                                forcePos = 0;
                                columns[2] = "Force";
                            }
                            break;
                        case "Resp":
                            if (!respChart.getData().contains(seriesResp)){
                                respChart.getData().add(seriesResp);
                                respPos = 0;
                                columns[2] = "Resp";
                            }
                            break;
                        case "Temp":
                            if (!tempChart.getData().contains(seriesTemp)){
                                tempChart.getData().add(seriesTemp);
                                tempPos = 0;
                                columns[2] = "Temp";
                            }
                            break; 
                        default:
                            System.err.println("Error with the ComboBox");
                    }
                }
            }
        });
        comboBox2.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(oldValue != null){
                    switch (oldValue){
                        case "ACC X":
                            accChart.getData().remove(seriesX);
                            break;
                        case "ACC Y":
                            accChart.getData().remove(seriesY);
                            break;
                        case "ACC Z":
                            accChart.getData().remove(seriesZ);
                            break;
                        case "ECG":
                            ecgChart.getData().remove(seriesEcg);
                            break;
                        case "EDA":
                            edaChart.getData().remove(seriesEda);
                            break;
                        case "EEG":
                            eegChart.getData().remove(seriesEeg);
                            break;
                        case "EMG":
                            emgChart.getData().remove(seriesEmg);
                            break;
                        case "Force":
                            forceChart.getData().remove(seriesForce);
                            break;
                        case "Resp":
                            respChart.getData().remove(seriesResp);
                            break;
                        case "Temp":
                            tempChart.getData().remove(seriesTemp);
                            break;
                    }
                }
                if (newValue != null){
                    switch (newValue){
                        case "-":
                            comboBox2.getSelectionModel().clearSelection();
                            break;
                        case "ACC X":
                            if (!accChart.getData().contains(seriesX)){
                                accChart.getData().add(seriesX);
                                accXPos = 1;
                                columns[3] = "ACCX";
                            }
                            break;
                        case "ACC Y":
                            if (!accChart.getData().contains(seriesY)){
                                accChart.getData().add(seriesY);
                                accYPos = 1;
                                columns[3] = "ACCY";
                            }
                            break;
                        case "ACC Z":
                            if (!accChart.getData().contains(seriesZ)){
                                accChart.getData().add(seriesZ);
                                accZPos = 1;
                                columns[3] = "ACCZ";
                            }
                            break;
                        case "ECG":
                            if (!ecgChart.getData().contains(seriesEcg)){
                                ecgChart.getData().add(seriesEcg);
                                ecgPos = 1;
                                columns[3] = "ECG";
                            }
                            break;
                        case "EDA":
                            if (!edaChart.getData().contains(seriesEda)){
                                edaChart.getData().add(seriesEda);
                                edaPos = 1;
                                columns[3] = "EDA";
                            }
                            break;
                        case "EEG":
                            if (!eegChart.getData().contains(seriesEeg)){
                                eegChart.getData().add(seriesEeg);
                                eegPos = 1;
                                columns[3] = "EEG";
                            }
                            break;
                        case "EMG":
                            if (!emgChart.getData().contains(seriesEmg)){
                                emgChart.getData().add(seriesEmg);
                                emgPos = 1;
                                columns[3] = "EMG";
                            }
                            break;
                        case "Force":
                            if (!forceChart.getData().contains(seriesForce)){
                                forceChart.getData().add(seriesForce);
                                forcePos = 1;
                                columns[3] = "Force";
                            }
                            break;
                        case "Resp":
                            if (!respChart.getData().contains(seriesResp)){
                                respChart.getData().add(seriesResp);
                                respPos = 1;
                                columns[3] = "Resp";
                            }
                            break;
                        case "Temp":
                            if (!tempChart.getData().contains(seriesTemp)){
                                tempChart.getData().add(seriesTemp);
                                tempPos = 1;
                                columns[3] = "Temp";
                            }
                            break; 
                        default:
                            System.err.println("Error with the ComboBox");
                    }
                }
            }
        });
        comboBox3.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(oldValue != null){
                    switch (oldValue){
                        case "ACC X":
                            accChart.getData().remove(seriesX);
                            break;
                        case "ACC Y":
                            accChart.getData().remove(seriesY);
                            break;
                        case "ACC Z":
                            accChart.getData().remove(seriesZ);
                            break;
                        case "ECG":
                            ecgChart.getData().remove(seriesEcg);
                            break;
                        case "EDA":
                            edaChart.getData().remove(seriesEda);
                            break;
                        case "EEG":
                            eegChart.getData().remove(seriesEeg);
                            break;
                        case "EMG":
                            emgChart.getData().remove(seriesEmg);
                            break;
                        case "Force":
                            forceChart.getData().remove(seriesForce);
                            break;
                        case "Resp":
                            respChart.getData().remove(seriesResp);
                            break;
                        case "Temp":
                            tempChart.getData().remove(seriesTemp);
                            break;
                    }
                }
                if (newValue != null){
                    switch (newValue){
                        case "-":
                            comboBox3.getSelectionModel().clearSelection();   
                            break;
                        case "ACC X":
                            if (!accChart.getData().contains(seriesX)){
                                accChart.getData().add(seriesX);
                                accXPos = 2;
                                columns[4] = "ACCX";
                            }
                            break;
                        case "ACC Y":
                            if (!accChart.getData().contains(seriesY)){
                                accChart.getData().add(seriesY);
                                accYPos = 2;
                                columns[4] = "ACCY";
                            }
                            break;
                        case "ACC Z":
                            if (!accChart.getData().contains(seriesZ)){
                                accChart.getData().add(seriesZ);
                                accZPos = 2;
                                columns[4] = "ACCZ";
                            }
                            break;
                        case "ECG":
                            if (!ecgChart.getData().contains(seriesEcg)){
                                ecgChart.getData().add(seriesEcg);
                                ecgPos = 2;
                                columns[4] = "ECG";
                            }
                            break;
                        case "EDA":
                            if (!edaChart.getData().contains(seriesEda)){
                                edaChart.getData().add(seriesEda);
                                edaPos = 2;
                                columns[4] = "EDA";
                            }
                            break;
                        case "EEG":
                            if (!eegChart.getData().contains(seriesEeg)){
                                eegChart.getData().add(seriesEeg);
                                eegPos = 2;
                                columns[4] = "EEG";
                            }
                            break;
                        case "EMG":
                            if (!emgChart.getData().contains(seriesEmg)){
                                emgChart.getData().add(seriesEmg);
                                emgPos = 2;
                                columns[4] = "EMG";
                            }
                            break;
                        case "Force":
                            if (!forceChart.getData().contains(seriesForce)){
                                forceChart.getData().add(seriesForce);
                                forcePos = 2;
                                columns[4] = "Force";
                            }
                            break;
                        case "Resp":
                            if (!respChart.getData().contains(seriesResp)){
                                respChart.getData().add(seriesResp);
                                respPos = 2;
                                columns[4] = "Resp";
                            }
                            break;
                        case "Temp":
                            if (!tempChart.getData().contains(seriesTemp)){
                                tempChart.getData().add(seriesTemp);
                                tempPos = 2;
                                columns[4] = "Temp";
                            }
                            break; 
                        default:
                            System.err.println("Error with the ComboBox");
                    }
                }
            }
        });
        comboBox4.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(oldValue != null){
                    switch (oldValue){
                        case "ACC X":
                            accChart.getData().remove(seriesX);
                            break;
                        case "ACC Y":
                            accChart.getData().remove(seriesY);
                            break;
                        case "ACC Z":
                            accChart.getData().remove(seriesZ);
                            break;
                        case "ECG":
                            ecgChart.getData().remove(seriesEcg);
                            break;
                        case "EDA":
                            edaChart.getData().remove(seriesEda);
                            break;
                        case "EEG":
                            eegChart.getData().remove(seriesEeg);
                            break;
                        case "EMG":
                            emgChart.getData().remove(seriesEmg);
                            break;
                        case "Force":
                            forceChart.getData().remove(seriesForce);
                            break;
                        case "Resp":
                            respChart.getData().remove(seriesResp);
                            break;
                        case "Temp":
                            tempChart.getData().remove(seriesTemp);
                            break;
                    }
                }
                if (newValue != null){
                    switch (newValue){
                        case "-":
                            comboBox4.getSelectionModel().clearSelection();   
                            break;
                        case "ACC X":
                            if (!accChart.getData().contains(seriesX)){
                                accChart.getData().add(seriesX);
                                accXPos = 3;
                                columns[5] = "ACCX";
                            }
                            break;
                        case "ACC Y":
                            if (!accChart.getData().contains(seriesY)){
                                accChart.getData().add(seriesY);
                                accYPos = 3;
                                columns[5] = "ACCY";
                            }
                            break;
                        case "ACC Z":
                            if (!accChart.getData().contains(seriesZ)){
                                accChart.getData().add(seriesZ);
                                accZPos = 3;
                                columns[5] = "ACCZ";
                            }
                            break;
                        case "ECG":
                            if (!ecgChart.getData().contains(seriesEcg)){
                                ecgChart.getData().add(seriesEcg);
                                ecgPos = 3;
                                columns[5] = "ECG";
                            }
                            break;
                        case "EDA":
                            if (!edaChart.getData().contains(seriesEda)){
                                edaChart.getData().add(seriesEda);
                                edaPos = 3;
                                columns[5] = "EDA";
                            }
                            break;
                        case "EEG":
                            if (!eegChart.getData().contains(seriesEeg)){
                                eegChart.getData().add(seriesEeg);
                                eegPos = 3;
                                columns[5] = "EEG";
                            }
                            break;
                        case "EMG":
                            if (!emgChart.getData().contains(seriesEmg)){
                                emgChart.getData().add(seriesEmg);
                                emgPos = 3;
                                columns[5] = "EMG";
                            }
                            break;
                        case "Force":
                            if (!forceChart.getData().contains(seriesForce)){
                                forceChart.getData().add(seriesForce);
                                forcePos = 3;
                                columns[5] = "Force";
                            }
                            break;
                        case "Resp":
                            if (!respChart.getData().contains(seriesResp)){
                                respChart.getData().add(seriesResp);
                                respPos = 3;
                                columns[5] = "Resp";
                            }
                            break;
                        case "Temp":
                            if (!tempChart.getData().contains(seriesTemp)){
                                tempChart.getData().add(seriesTemp);
                                tempPos = 3;
                                columns[5] = "Temp";
                            }
                            break; 
                        default:
                            System.err.println("Error with the ComboBox");
                    }
                }
            }
        });
        comboBox5.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(oldValue != null){
                    switch (oldValue){
                        case "ACC X":
                            accChart.getData().remove(seriesX);
                            break;
                        case "ACC Y":
                            accChart.getData().remove(seriesY);
                            break;
                        case "ACC Z":
                            accChart.getData().remove(seriesZ);
                            break;
                        case "ECG":
                            ecgChart.getData().remove(seriesEcg);
                            break;
                        case "EDA":
                            edaChart.getData().remove(seriesEda);
                            break;
                        case "EEG":
                            eegChart.getData().remove(seriesEeg);
                            break;
                        case "EMG":
                            emgChart.getData().remove(seriesEmg);
                            break;
                        case "Force":
                            forceChart.getData().remove(seriesForce);
                            break;
                        case "Resp":
                            respChart.getData().remove(seriesResp);
                            break;
                        case "Temp":
                            tempChart.getData().remove(seriesTemp);
                            break;
                    }
                }
                if (newValue != null){
                    switch (newValue){
                        case "-":
                            comboBox5.getSelectionModel().clearSelection();   
                            break;
                        case "ACC X":
                            if (!accChart.getData().contains(seriesX)){
                                accChart.getData().add(seriesX);
                                accXPos = 4;
                                columns[6] = "ACCX";
                            }
                            break;
                        case "ACC Y":
                            if (!accChart.getData().contains(seriesY)){
                                accChart.getData().add(seriesY);
                                accYPos = 4;
                                columns[6] = "ACCY";
                            }
                            break;
                        case "ACC Z":
                            if (!accChart.getData().contains(seriesZ)){
                                accChart.getData().add(seriesZ);
                                accZPos = 4;
                                columns[6] = "ACCZ";
                            }
                            break;
                        case "ECG":
                            if (!ecgChart.getData().contains(seriesEcg)){
                                ecgChart.getData().add(seriesEcg);
                                ecgPos = 4;
                                columns[6] = "ECG";
                            }
                            break;
                        case "EDA":
                            if (!edaChart.getData().contains(seriesEda)){
                                edaChart.getData().add(seriesEda);
                                edaPos = 4;
                                columns[6] = "EDA";
                            }
                            break;
                        case "EEG":
                            if (!eegChart.getData().contains(seriesEeg)){
                                eegChart.getData().add(seriesEeg);
                                eegPos = 4;
                                columns[6] = "EEG";
                            }
                            break;
                        case "EMG":
                            if (!emgChart.getData().contains(seriesEmg)){
                                emgChart.getData().add(seriesEmg);
                                emgPos = 4;
                                columns[6] = "EMG";
                            }
                            break;
                        case "Force":
                            if (!forceChart.getData().contains(seriesForce)){
                                forceChart.getData().add(seriesForce);
                                forcePos = 4;
                                columns[6] = "Force";
                            }
                            break;
                        case "Resp":
                            if (!respChart.getData().contains(seriesResp)){
                                respChart.getData().add(seriesResp);
                                respPos = 4;
                                columns[6] = "Resp";
                            }
                            break;
                        case "Temp":
                            if (!tempChart.getData().contains(seriesTemp)){
                                tempChart.getData().add(seriesTemp);
                                tempPos = 4;
                                columns[6] = "Temp";
                            }
                            break; 
                        default:
                            System.err.println("Error with the ComboBox");
                    }
                }
            }
        });
        comboBox6.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(oldValue != null){
                    switch (oldValue){
                        case "ACC X":
                            accChart.getData().remove(seriesX);
                            break;
                        case "ACC Y":
                            accChart.getData().remove(seriesY);
                            break;
                        case "ACC Z":
                            accChart.getData().remove(seriesZ);
                            break;
                        case "ECG":
                            ecgChart.getData().remove(seriesEcg);
                            break;
                        case "EDA":
                            edaChart.getData().remove(seriesEda);
                            break;
                        case "EEG":
                            eegChart.getData().remove(seriesEeg);
                            break;
                        case "EMG":
                            emgChart.getData().remove(seriesEmg);
                            break;
                        case "Force":
                            forceChart.getData().remove(seriesForce);
                            break;
                        case "Resp":
                            respChart.getData().remove(seriesResp);
                            break;
                        case "Temp":
                            tempChart.getData().remove(seriesTemp);
                            break;
                    }
                }
                if (newValue != null){
                    switch (newValue){
                        case "-":
                            comboBox6.getSelectionModel().clearSelection();   
                            break;
                        case "ACC X":
                            if (!accChart.getData().contains(seriesX)){
                                accChart.getData().add(seriesX);
                                accXPos = 5;
                                columns[7] = "ACCX";
                            }
                            break;
                        case "ACC Y":
                            if (!accChart.getData().contains(seriesY)){
                                accChart.getData().add(seriesY);
                                accYPos = 5;
                                columns[7] = "ACCY";
                            }
                            break;
                        case "ACC Z":
                            if (!accChart.getData().contains(seriesZ)){
                                accChart.getData().add(seriesZ);
                                accZPos = 5;
                                columns[7] = "ACCZ";
                            }
                            break;
                        case "ECG":
                            if (!ecgChart.getData().contains(seriesEcg)){
                                ecgChart.getData().add(seriesEcg);
                                ecgPos = 5;
                                columns[7] = "ECG";
                            }
                            break;
                        case "EDA":
                            if (!edaChart.getData().contains(seriesEda)){
                                edaChart.getData().add(seriesEda);
                                edaPos = 5;
                                columns[7] = "EDA";
                            }
                            break;
                        case "EEG":
                            if (!eegChart.getData().contains(seriesEeg)){
                                eegChart.getData().add(seriesEeg);
                                eegPos = 5;
                                columns[7] = "EEG";
                            }
                            break;
                        case "EMG":
                            if (!emgChart.getData().contains(seriesEmg)){
                                emgChart.getData().add(seriesEmg);
                                emgPos = 5;
                                columns[7] = "EMG";
                            }
                            break;
                        case "Force":
                            if (!forceChart.getData().contains(seriesForce)){
                                forceChart.getData().add(seriesForce);
                                forcePos = 5;
                                columns[7] = "Force";
                            }
                            break;
                        case "Resp":
                            if (!respChart.getData().contains(seriesResp)){
                                respChart.getData().add(seriesResp);
                                respPos = 5;
                                columns[7] = "Resp";
                            }
                            break;
                        case "Temp":
                            if (!tempChart.getData().contains(seriesTemp)){
                                tempChart.getData().add(seriesTemp);
                                tempPos = 5;
                                columns[7] = "Temp";
                            }
                            break; 
                        default:
                            System.err.println("Error with the ComboBox");
                    }
                }
            }
        });
        comboBox7.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(oldValue != null){
                    switch (oldValue){
                        case "ACC X":
                            accChart.getData().remove(seriesX);
                            break;
                        case "ACC Y":
                            accChart.getData().remove(seriesY);
                            break;
                        case "ACC Z":
                            accChart.getData().remove(seriesZ);
                            break;
                        case "ECG":
                            ecgChart.getData().remove(seriesEcg);
                            break;
                        case "EDA":
                            edaChart.getData().remove(seriesEda);
                            break;
                        case "EEG":
                            eegChart.getData().remove(seriesEeg);
                            break;
                        case "EMG":
                            emgChart.getData().remove(seriesEmg);
                            break;
                        case "Force":
                            forceChart.getData().remove(seriesForce);
                            break;
                        case "Resp":
                            respChart.getData().remove(seriesResp);
                            break;
                        case "Temp":
                            tempChart.getData().remove(seriesTemp);
                            break;
                    }
                }
                if (newValue != null){
                    switch (newValue){
                        case "-":
                            comboBox7.getSelectionModel().clearSelection();   
                            break;
                        case "ACC X":
                            if (!accChart.getData().contains(seriesX)){
                                accChart.getData().add(seriesX);
                                accXPos = 6;
                                columns[8] = "ACCX";
                            }
                            break;
                        case "ACC Y":
                            if (!accChart.getData().contains(seriesY)){
                                accChart.getData().add(seriesY);
                                accYPos = 6;
                                columns[8] = "ACCY";
                            }
                            break;
                        case "ACC Z":
                            if (!accChart.getData().contains(seriesZ)){
                                accChart.getData().add(seriesZ);
                                accZPos = 6;
                                columns[8] = "ACCZ";
                            }
                            break;
                        case "ECG":
                            if (!ecgChart.getData().contains(seriesEcg)){
                                ecgChart.getData().add(seriesEcg);
                                ecgPos = 6;
                                columns[8] = "ECG";
                            }
                            break;
                        case "EDA":
                            if (!edaChart.getData().contains(seriesEda)){
                                edaChart.getData().add(seriesEda);
                                edaPos = 6;
                                columns[8] = "EDA";
                            }
                            break;
                        case "EEG":
                            if (!eegChart.getData().contains(seriesEeg)){
                                eegChart.getData().add(seriesEeg);
                                eegPos = 6;
                                columns[8] = "EEG";
                            }
                            break;
                        case "EMG":
                            if (!emgChart.getData().contains(seriesEmg)){
                                emgChart.getData().add(seriesEmg);
                                emgPos = 6;
                                columns[8] = "EMG";
                            }
                            break;
                        case "Force":
                            if (!forceChart.getData().contains(seriesForce)){
                                forceChart.getData().add(seriesForce);
                                forcePos = 6;
                                columns[8] = "Force";
                            }
                            break;
                        case "Resp":
                            if (!respChart.getData().contains(seriesResp)){
                                respChart.getData().add(seriesResp);
                                respPos = 6;
                                columns[8] = "Resp";
                            }
                            break;
                        case "Temp":
                            if (!tempChart.getData().contains(seriesTemp)){
                                tempChart.getData().add(seriesTemp);
                                tempPos = 6;
                                columns[8] = "Temp";
                            }
                            break; 
                        default:
                            System.err.println("Error with the ComboBox");
                    }
                }
            }
        });
        comboBox8.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(oldValue != null){
                    switch (oldValue){
                        case "ACC X":
                            accChart.getData().remove(seriesX);
                            break;
                        case "ACC Y":
                            accChart.getData().remove(seriesY);
                            break;
                        case "ACC Z":
                            accChart.getData().remove(seriesZ);
                            break;
                        case "ECG":
                            ecgChart.getData().remove(seriesEcg);
                            break;
                        case "EDA":
                            edaChart.getData().remove(seriesEda);
                            break;
                        case "EEG":
                            eegChart.getData().remove(seriesEeg);
                            break;
                        case "EMG":
                            emgChart.getData().remove(seriesEmg);
                            break;
                        case "Force":
                            forceChart.getData().remove(seriesForce);
                            break;
                        case "Resp":
                            respChart.getData().remove(seriesResp);
                            break;
                        case "Temp":
                            tempChart.getData().remove(seriesTemp);
                            break;
                    }
                }
                if (newValue != null){
                    switch (newValue){
                        case "-":
                            comboBox8.getSelectionModel().clearSelection();   
                            break;
                        case "ACC X":
                            if (!accChart.getData().contains(seriesX)){
                                accChart.getData().add(seriesX);
                                accXPos = 7;
                                columns[9] = "ACCX";
                            }
                            break;
                        case "ACC Y":
                            if (!accChart.getData().contains(seriesY)){
                                accChart.getData().add(seriesY);
                                accYPos = 7;
                                columns[9] = "ACCY";
                            }
                            break;
                        case "ACC Z":
                            if (!accChart.getData().contains(seriesZ)){
                                accChart.getData().add(seriesZ);
                                accZPos = 7;
                                columns[9] = "ACCZ";
                            }
                            break;
                        case "ECG":
                            if (!ecgChart.getData().contains(seriesEcg)){
                                ecgChart.getData().add(seriesEcg);
                                ecgPos = 7;
                                columns[9] = "ECG";
                            }
                            break;
                        case "EDA":
                            if (!edaChart.getData().contains(seriesEda)){
                                edaChart.getData().add(seriesEda);
                                edaPos = 7;
                                columns[9] = "EDA";
                            }
                            break;
                        case "EEG":
                            if (!eegChart.getData().contains(seriesEeg)){
                                eegChart.getData().add(seriesEeg);
                                eegPos = 7;
                                columns[9] = "EEG";
                            }
                            break;
                        case "EMG":
                            if (!emgChart.getData().contains(seriesEmg)){
                                emgChart.getData().add(seriesEmg);
                                emgPos = 7;
                                columns[9] = "EMG";
                            }
                            break;
                        case "Force":
                            if (!forceChart.getData().contains(seriesForce)){
                                forceChart.getData().add(seriesForce);
                                forcePos = 7;
                                columns[9] = "Force";
                            }
                            break;
                        case "Resp":
                            if (!respChart.getData().contains(seriesResp)){
                                respChart.getData().add(seriesResp);
                                respPos = 7;
                                columns[9] = "Resp";
                            }
                            break;
                        case "Temp":
                            if (!tempChart.getData().contains(seriesTemp)){
                                tempChart.getData().add(seriesTemp);
                                tempPos = 7;
                                columns[9] = "Temp";
                            }
                            break; 
                        default:
                            System.err.println("Error with the ComboBox");
                    }
                }
            }
        });


        recordButton.setOnMouseClicked((MouseEvent event) -> {
            if (record){
                logger.info("Stop recording");
                recordButton.setText("Start recording");
                synchronizeButton.setDisable(false);
                try {
                    stopRecording();
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, null, ex);
                } 
            }
            else{
                logger.info("Start recording");
                if (columns[2] == null && columns[3] == null && columns[4] == null && columns[5] == null &&
                        columns[6] == null && columns[7] == null && columns[8] == null && columns[9] == null){
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Confirmation Dialog");
                    alert.setHeaderText("Start recording");
                    alert.setContentText("Start recording without configuring the signals input?");

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK){
                        recordButton.setText("Stop recording");
                        startRecording();
                    } else {
                        // do nothing
                    }
                }
                else {
                    recordButton.setText("Stop recording");
                    startRecording();
                }
            }
        });

        synchronizeButton.setOnMouseClicked((MouseEvent event) -> {
//            synchronize();
        });
        

        initializeCamera();
        
        initializeSignals();
        
    }
    
    private void createFolder() throws IOException{
        Date now = Calendar.getInstance().getTime();
        String nowString = new SimpleDateFormat("yyyyMMdd_HHmmss").format(now);
        currentFolder = appFolder + "\\" + nowString;
        File dir = new File(currentFolder);
        dir.mkdir();

        Gson gson = new Gson();
        JsonObject propertiesObject = new JsonObject();
        propertiesObject.addProperty(PATH, currentFolder);
        propertiesObject.addProperty(CREATED_BY, JAVA);
        propertiesObject.addProperty(USED, FALSE);

        String propertiesString = gson.toJson(propertiesObject);
        FileWriter writer = new FileWriter(configFile);
        writer.write(propertiesString);
        writer.close();
    }
    
    private void initializeCamera(){
        GridPane.setHalignment(cameraView, HPos.CENTER);
        
        grabber = new OpenCVFrameGrabber(CAMERA_ID);
        converter = new Java2DFrameConverter();
        
        try {
            grabber.start();
        } catch (FrameGrabber.Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        
        recorder = new FFmpegFrameRecorder(
                cameraVideoPathTemp,
                captureWidth, captureHeight, 2);
        recorder.setInterleaved(true);
        recorder.setVideoOption("tune", "zerolatency");
        recorder.setVideoOption("preset", "ultrafast");
        recorder.setVideoOption("crf", "28");
        recorder.setVideoBitrate(2000000);
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
        recorder.setFormat(MP4);
        recorder.setFrameRate(FRAME_RATE);
        recorder.setGopSize(GOP_LENGTH_IN_FRAMES);

        recorder.setAudioOption("crf", "0");
        recorder.setAudioQuality(0);
        recorder.setAudioBitrate(192000);
        recorder.setSampleRate(44100);
        recorder.setAudioChannels(2);
        recorder.setAudioCodec(avcodec.AV_CODEC_ID_AAC);

        cameraTimer = Executors.newSingleThreadScheduledExecutor();
        cameraTimer.scheduleAtFixedRate(new CameraRunnable(), 0, 33, TimeUnit.MILLISECONDS);
        
    }
    
    private void initializeSignals() {
        
        Vector devs = new Vector();
        
        try {
            Device.FindDevices(devs);          
            
            if (devs.isEmpty()){
                Alert alert = new Alert(AlertType.ERROR, "Impossible to connect to the sensors. "
                        + "Please verify the connection and try again.");
                alert.setTitle("Sensors not found");
                alert.showAndWait();
            }
            else {
                deviceMAC = (String) devs.get(0);

                dev = new Device(deviceMAC); // MAC address of physical device

                frames = new Device.Frame[1];
                frames[0] = new Device.Frame();

                dev.BeginAcq(SAMPLING_RATE, 0xFF, N_BITS);
                
            }
        }
        catch (BPException err){
            logger.log(Level.SEVERE, null, err + " - " + err.getMessage()); 
        }   
        
        if (!devs.isEmpty()){
            try {
                signalsWriter = new FileWriter(signalsPathTemp);
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        
            counter = new int[1];

            Thread getSignals = new Thread(() -> {

                try {
                    dev.GetFrames(1, frames);
                    if (record){
                        Device.Frame frame = frames[0];
                        String output = counter[0] + "\t" + frame.dig_in + "\t";
                        int length = frame.an_in.length;
                        for (int j = 0; j < length-1; j++){
                            output += frame.an_in[j] + "\t";
                        }
                        output += frame.an_in[length-1];

                        signalsWriter.write(output + System.lineSeparator());
                        counter[0]++;                  
                    }
                } catch (BPException ex) {
                    logger.log(Level.SEVERE, null, ex + " - " + ex.getMessage());
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, "IOException in getSignals: " + ex.getMessage(), ex);
                }
            });

            signalsTimer = Executors.newSingleThreadScheduledExecutor();
            signalsTimer.scheduleAtFixedRate(getSignals, 0, 1, TimeUnit.MILLISECONDS);
        
            int[] index = {0};

            updateGraphs = new Timeline(new KeyFrame(Duration.millis(100), (ActionEvent event) -> {
                try {
                    if (seriesTemp.getData().size() >= 10){
                        seriesX.getData().remove(0);
                        seriesY.getData().remove(0);
                        seriesZ.getData().remove(0);
                        seriesTemp.getData().remove(0);
                        seriesEcg.getData().remove(0);
                        seriesEda.getData().remove(0);
                        seriesEeg.getData().remove(0);
                        seriesEmg.getData().remove(0);
                        seriesResp.getData().remove(0);
                        seriesForce.getData().remove(0);
                    }

                    if (signalsCounter % 10 == 0){
                        index[0] = 0;
                    }


                    Device.Frame data = frames[0];


                    String output = counter[0] + "\t" + data.dig_in + "\t";
                    int length = data.an_in.length;
                    for (int j = 0; j < length-1; j++){
                        output += data.an_in[j] + "\t";
                    }
                    output += data.an_in[length-1];



                    double temp = round(transTemp(data.an_in[tempPos]),1);

                    seriesX.getData().add(new XYChart.Data<>(signalsCounter + "", transAcc(data.an_in[accXPos])));
                    seriesEeg.getData().add(new XYChart.Data<>(signalsCounter + "", transEeg(data.an_in[eegPos])));
                    seriesY.getData().add(new XYChart.Data<>(signalsCounter + "", transAcc(data.an_in[accYPos])));
                    seriesEcg.getData().add(new XYChart.Data<>(signalsCounter + "", transEcg(data.an_in[ecgPos])));
                    seriesZ.getData().add(new XYChart.Data<>(signalsCounter + "", transAcc(data.an_in[accZPos])));
                    seriesTemp.getData().add(new XYChart.Data<>(signalsCounter + "", temp));
                    seriesResp.getData().add(new XYChart.Data<>(signalsCounter + "", transResp(data.an_in[respPos])));
                    seriesForce.getData().add(new XYChart.Data<>(signalsCounter + "", transForce(data.an_in[forcePos])));
                    seriesEmg.getData().add(new XYChart.Data<>(signalsCounter + "", transEmg(data.an_in[emgPos])));
                    seriesEda.getData().add(new XYChart.Data<>(signalsCounter + "", transEda(data.an_in[edaPos])));

                    signalsCounter++;
                    index[0]++;

                } catch (Exception ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }));
            updateGraphs.setCycleCount(Timeline.INDEFINITE);
            updateGraphs.play();
        }
    }
    
    
    private void startRecording() {
        try {
            recorder.start();
            record = true;
        } catch (FrameRecorder.Exception ex) {
            logger.log(Level.SEVERE, "Exception with FrameRecorder", ex);
        }
    }
    
    private void stopRecording() throws FrameRecorder.Exception, FileNotFoundException, IOException, ImageProcessingException {
        record = false;
        recorder.stop(); 
        signalsWriter.close();
        updateController();
    }
    
    @FXML
    private void switchScene(ActionEvent event) throws IOException{
        stage = (Stage) cameraView.getScene().getWindow();
        BorderPane root = FXMLLoader.load(getClass().getResource("/fxml/OfflineScene.fxml"));
        stage.getScene().setRoot(root);
    }
    
    private void updateController() throws FileNotFoundException, IOException, ImageProcessingException{
        File videoFile = new File(cameraVideoPathTemp);
        if (!videoFile.exists()) {
            throw new FileNotFoundException("File " + cameraVideoPathTemp + " not exists");
        }
        if (!videoFile.canRead()) {
            throw new IllegalStateException("No read permissions to file " + cameraVideoPathTemp);
        }
        
        long size = 0;
        long duration = 0;
        
        FileChannel fc = new FileInputStream(cameraVideoPathTemp).getChannel();
        IsoFile isoFile = new IsoFile(fc);
        MovieBox moov = isoFile.getMovieBox();
        for(Box b : moov.getBoxes()) {
            if (b.toString().startsWith("MovieHeaderBox")){
                String box = b.toString();
                String durationString = box.substring(box.indexOf("duration")+9, box.indexOf(";rate"));
                duration = Long.parseLong(durationString);
            }
        }
        
        Metadata metadata = ImageMetadataReader.readMetadata(videoFile);
        for (Directory directory : metadata.getDirectories()) {
            if (directory.getName().equals("File")){
                for (Tag tag : directory.getTags()) {
                    if (tag.getTagName().equals("File Size")){
                        String sizeString = tag.getDescription();
                        sizeString = sizeString.substring(0,sizeString.indexOf(" "));
                        size = Long.parseLong(sizeString);
                    }
                }
            }
        }
        
        long signalsDuration = (long)(counter[0]/SAMPLING_RATE*1000); //milliseconds
        
        SignalsMetadata signalsMetadata = new SignalsMetadata(timestamp.getTime(), SAMPLING_RATE, signalsDuration, counter[0], signalsPath);
        Gson gson = new Gson();
        JsonElement signalsElement = gson.toJsonTree(signalsMetadata);      
        
        CameraVideoMetadata cameraVideoMetadata = new CameraVideoMetadata(timestamp.getTime(), FRAME_RATE, duration, size, MP4, cameraVideoPath);
        JsonElement cameraElement = gson.toJsonTree(cameraVideoMetadata);

        String jsonString;
        if (storedJsonString == null || storedJsonString.equals("")){
            JsonObject newObject = new JsonObject();
            newObject.add(CAMERA_VIDEO, cameraElement);
            newObject.add(SIGNALS, signalsElement);
            jsonString = gson.toJson(newObject);
        }
        else {
            JsonObject currentJson = gson.fromJson(storedJsonString, JsonObject.class);
            currentJson.add(CAMERA_VIDEO, cameraElement);
            currentJson.add(SIGNALS, signalsElement);
            jsonString = gson.toJson(currentJson);
        }
        
        BufferedWriter bw = new BufferedWriter(new FileWriter(controllerCamSigPath));
        bw.write(jsonString);
        bw.newLine();
        bw.close();
        
        
                
            
//        FFmpeg ffmpeg = new FFmpeg("C:\\Users\\danilo.bernardi\\Downloads\\ffmpeg-20180319-e4eaaf7-win64-shared\\bin\\ffmpeg.exe");
//        System.out.println("VERSION: " + ffmpeg.version());
//        
//        FFmpegBuilder builder = new FFmpegBuilder()
//                .addExtraArgs("-ss", "00:00:07.000")
//                .addInput("C:\\Users\\danilo.bernardi\\Documents\\VRPhysioPlatform\\20180314_164518\\Video 8.wmv")
////                .setInput("C:\\Users\\danilo.bernardi\\Documents\\VRPhysioPlatform\\20180314_164518\\Video 8.wmv")
//                
//                .addExtraArgs("-t", "00:00:07.000")
//                .addExtraArgs("-c:v copy")
//                .addOutput("C:\\Users\\danilo.bernardi\\Documents\\VRPhysioPlatform\\20180314_164518\\videoCut5.wmv")
//
//                .done();
//        List<String> args = builder.build();
//        System.out.println(args.toString());
//        FFmpegExecutor executor = new FFmpegExecutor(ffmpeg);
//        executor.createJob(builder).run();
       
    }
    
    private void updateSignalsFile(){
        BufferedWriter writer = null;
        BufferedReader br = null;
        String newPath = currentFolder + "\\" + "signals_temp.txt";
        System.out.println("Updating signals file...");
        try {
            
            writer = new BufferedWriter(new FileWriter(newPath));
            
            columns[0] = "seqNumber";
            columns[1] = "digitalInput";
            SignalsConfiguration signalsConfiguration = new SignalsConfiguration(deviceMAC, SAMPLING_RATE, columns, timestamp.getTime());
            
            Gson gson = new Gson();
            String jsonSignals = gson.toJson(signalsConfiguration);

            writer.write("# " + jsonSignals + System.lineSeparator());
            
            br = new BufferedReader(new FileReader(signalsPath));
            String line;
            while ((line = br.readLine()) != null) {
                writer.write(line);
                writer.newLine();  
            }
            
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
                br.close();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        File oldFile = new File(signalsPath);
        File newFile = new File(newPath);
        try {
            Files.move(newFile.toPath(), oldFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        System.out.println("...done");
    }
    
    private void synchronize(String directory){
        String controllerVrPath = directory + "\\" + CONTROLLER_VR_FILE;
        String controllerCamSigPath = directory + "\\" + CONTROLLER_CAMSIG_FILE;
        

        synchronizeButton.setDisable(true);
        syncLabel.setVisible(true);
        progressBar.setVisible(true);

        String controllerVr = readFile(controllerVrPath);
        Gson gson = new Gson();
        JsonObject controllerVrObject = gson.fromJson(controllerVr, JsonObject.class);
        JsonObject vrVideo = controllerVrObject.get(VR_VIDEO).getAsJsonObject();
        
        String controllerCamSig = readFile(controllerCamSigPath);
        JsonObject controllerCamSigObject = gson.fromJson(controllerCamSig, JsonObject.class);
        JsonObject cameraVideo = controllerCamSigObject.get(CAMERA_VIDEO).getAsJsonObject();
        
        long cameraVideoTs = cameraVideo.get(TIMESTAMP).getAsLong();
        long vrVideoTs = vrVideo.get(TIMESTAMP).getAsLong();
        
        double vrVideoDuration = vrVideo.get(DURATION).getAsDouble();
        double cameraVideoDuration = cameraVideo.get(DURATION).getAsDouble();

        double diff;
        
        if (cameraVideoTs > vrVideoTs){
            diff = cameraVideoTs - vrVideoTs;
            
            double start = diff/1000;
                        
            double vrNewDuration = vrVideoDuration-diff;
            if (vrNewDuration > cameraVideoDuration){ 
                print("VR starts first and lasts more");
                // cut vr video both in the beginning and in the end
                double end = cameraVideoDuration/1000;
                try {
                    vrVideoCut = new Thread(new CutVideo(VR_VIDEO, vrVideoPathTemp, vrVideoPath, start, end));
                    vrVideoCut.start();

                    // wait for the completion without blocking the main thread
                    new Thread(() -> {
                        while(true){
                            // without this sleep it doesn't work
                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException ex) {
                                logger.log(Level.SEVERE, null, ex);
                            }
                            if (cutVrCompleted){
                                print("CUT VR COMPLETED");
                                progressBar.setVisible(false);
                                syncLabel.setVisible(false);
                                
                                // delete temp vr video
                                boolean deleteResult = new File(vrVideoPathTemp).delete();
                                if (!deleteResult){
                                    logger.log(Level.SEVERE, "Deletion of temp vr video failed");
                                }
                                    
                                // rename camera video
                                Path cameraTempPath = Paths.get(cameraVideoPathTemp);
                                Path cameraPath = Paths.get(cameraVideoPath);
                                try {
                                    Files.move(cameraTempPath, cameraPath);
                                } catch (IOException ex) {
                                    logger.log(Level.SEVERE, null, ex);
                                }
                                print("DONE.");
                                return;
                            }
                        }
                    }).start();
                    
                    
                    
                    // copy signals file (need to insert configuration at the beginning)
                    cutSignals(0, end*1000);
                    
                    // delete temp signals file
                    boolean deleteResult = new File(signalsPathTemp).delete();
                    if (!deleteResult){
                        logger.log(Level.SEVERE, "Deletion of temp signals file failed");
                    }
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, null, ex);
                } 
            }
            else{
                System.out.println("VR starts first and lasts less");
                // cut vr video in the beginning and camera video in the end
                double end = vrNewDuration/1000;
                try {
                    // cut vr video
                    vrVideoCut = new Thread(new CutVideo(VR_VIDEO, vrVideoPathTemp, vrVideoPath, start, end));
                    vrVideoCut.start();
//                    cutVideo(start, end, vrVideoPathTemp, vrVideoPath);

                    // cut camera video
                    cameraVideoCut = new Thread(new CutVideo(CAMERA_VIDEO, cameraVideoPathTemp, cameraVideoPath, 0, end));
                    cameraVideoCut.start();
//                    cutVideo(0, end, cameraVideoPathTemp, cameraVideoPath);  

                    // wait for the completion without blocking the main thread
                    new Thread(() -> {
                        while(true){
                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException ex) {
                                logger.log(Level.SEVERE, null, ex);
                            }
                            
                            if (cutVrCompleted && cutCameraCompleted){
                                progressBar.setVisible(false);
                                syncLabel.setVisible(false);
                                
                                // delete temp vr video
                                boolean deleteResult = new File(vrVideoPathTemp).delete();
                                if (!deleteResult){
                                    logger.log(Level.SEVERE, "Deletion of temp vr video failed");
                                }

                                // delete temp camera video
                                deleteResult = new File(cameraVideoPathTemp).delete();
                                if (!deleteResult){
                                    logger.log(Level.SEVERE, "Deletion of temp camera video failed");
                                }
                                print("DONE.");
                                return;
                            }
                        }
                    }).start();


                    // cut signals file
                    cutSignals(0, end*1000);
                    
                    // delete temp signals file
                    boolean deleteResult = new File(signalsPathTemp).delete();
                    if (!deleteResult){
                        logger.log(Level.SEVERE, "Deletion of temp signals file failed");
                    }
                    
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, null, ex);
                } 
            }
   
        }
        else {
            diff = vrVideoTs - cameraVideoTs;
            
            double start = diff/1000;

            double cameraNewDuration = cameraVideoDuration-diff;
            if (cameraNewDuration > vrVideoDuration){
                System.out.println("Camera starts first and lasts more");
                // cut camera video both in the beginning and in the end
                double end = vrVideoDuration/1000;
                try {
                    cameraVideoCut = new Thread(new CutVideo(CAMERA_VIDEO, cameraVideoPathTemp, cameraVideoPath, start, end));
                    cameraVideoCut.start();

//                    cutVideo(start, end, cameraVideoPathTemp, cameraVideoPath);

                    // wait for the completion without blocking the main thread
                    new Thread(() -> {
                        while(true){
                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException ex) {
                                logger.log(Level.SEVERE, null, ex);
                            }
                            if (cutCameraCompleted){
                                print("CUT CAMERA COMPLETED");
                                progressBar.setVisible(false);
                                syncLabel.setVisible(false);
                                
                                boolean deleteResult = new File(cameraVideoPathTemp).delete();
                                if (!deleteResult){
                                    logger.log(Level.SEVERE, "Deletion of temp camera video failed");
                                }
                                Path vrTempPath = Paths.get(vrVideoPathTemp);
                                Path vrPath = Paths.get(vrVideoPath);
                                try {
                                    Files.move(vrTempPath, vrPath);
                                } catch (IOException ex) {
                                    logger.log(Level.SEVERE, null, ex);
                                }
                                print("DONE.");
                                return;
                            }
                        }
                    }).start();


                    
                    
                    // cut signals file
                    cutSignals(start*1000, end*1000);
                    
                    // delete temp signals file
                    boolean deleteResult = new File(signalsPathTemp).delete();
                    if (!deleteResult){
                        logger.log(Level.SEVERE, "Deletion of temp signals file failed");
                    }
                    
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, null, ex);
                } 
            }
            else {
                System.out.println("Camera starts first and lasts less");
                // cut camera video in the beginning and vr video in the end
                double end = cameraNewDuration/1000;
                try {
                    cameraVideoCut = new Thread(new CutVideo(CAMERA_VIDEO, cameraVideoPathTemp, cameraVideoPath, start, end));
                    cameraVideoCut.start();
//                    cutVideo(start, end, cameraVideoPathTemp, cameraVideoPath);

                    vrVideoCut = new Thread(new CutVideo(VR_VIDEO, vrVideoPathTemp, vrVideoPath, 0, end));
                    vrVideoCut.start();
//                    cutVideo(0, end, vrVideoPathTemp, vrVideoPath);

                    
                    // wait for the completion without blocking the main thread
                    new Thread(() -> {
                        while(true){
                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException ex) {
                                logger.log(Level.SEVERE, null, ex);
                            }
                            if (cutVrCompleted)
                                print("CUT VR COMPLETED");
                            if (cutCameraCompleted)
                                print("CUT CAMERA COMPLETED");
                            if (cutVrCompleted && cutCameraCompleted){
                                progressBar.setVisible(false);
                                syncLabel.setVisible(false);
                                
                                boolean deleteResult = new File(cameraVideoPathTemp).delete();
                                if (!deleteResult){
                                    logger.log(Level.SEVERE, "Deletion of temp camera video failed");
                                }

                                deleteResult = new File(vrVideoPathTemp).delete();
                                if (!deleteResult){
                                    logger.log(Level.SEVERE, "Deletion of temp vr video failed");
                                }
                                print("DONE.");
                                return;
                            }
                        }
                    }).start();



                    
                    
                    // cut signals file
                    cutSignals(start*1000, end*1000);
                    
                    // delete temp signals file
                    boolean deleteResult = new File(signalsPathTemp).delete();
                    if (!deleteResult){
                        logger.log(Level.SEVERE, "Deletion of temp signals file failed");
                    }
                    
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, null, ex);
                } 
            }

        }
        
//        // delete controller files
//        boolean deleteResult = new File(controllerCamSig).delete();
//        if (!deleteResult){
//            logger.log(Level.WARNING, "Deletion of controllerCamSig failed");
//        }
//        deleteResult = new File(controllerVr).delete();
//        if (!deleteResult){
//            logger.log(Level.WARNING, "Deletion of controllerVr failed");
//        }
        
    }
    
//    private void cutVideo(double start, double end, String videoPathIn, String videoPathOut) throws IllegalArgumentException, IOException, InterruptedException{
////	String cmd = FFMPEG + " -i \"" + videoPathIn +  "\" -ss " + start + " -c copy -to " + end + " -y " + videoPathOut;
//	String cmd = ffmpeg + " -i " + videoPathIn +  " -ss " + start + " -t " + end + " -y " + videoPathOut;
////	String cmd = FFMPEG + " -i \"" + videoPathIn +  "\" -ss " + start + " -y " + videoPathOut;
//        System.out.println(cmd);
////	Runtime runtime = Runtime.getRuntime();
//        System.out.println("ENCODING STARTED...");
////	Process p = runtime.exec(cmd);
////	p.waitFor();
//        
//        
//        
////        ProcessBuilder pb = new ProcessBuilder(cmd);
////        pb.redirectErrorStream(); // This will make both stdout and stderr be redirected to process.getInputStream();
////        pb.start();
//        
//        Process processDuration = new ProcessBuilder(
//                ffmpeg,
//                "-i",
//                videoPathIn,
//                "-ss",
//                String.valueOf(start),
//                "-t",
//                String.valueOf(end),
//                "-y",
//                videoPathOut)
//                .redirectErrorStream(true).start();
//        
//        StringBuilder strBuild = new StringBuilder();
//        try (BufferedReader processOutputReader = new BufferedReader(new InputStreamReader(processDuration.getInputStream(), Charset.defaultCharset()));) {
//            String line;
//            while ((line = processOutputReader.readLine()) != null) {
//                strBuild.append(line + System.lineSeparator());
//            }
//            processDuration.waitFor();
//        }
//        String outputJson = strBuild.toString().trim();
//        System.out.println(outputJson);
//        
//        System.out.println("DONE");
//    }
    
    private void prepareVR(){
        
    }
    
    private void cutSignals(double start, double end){
        BufferedWriter writer = null;
        BufferedReader br = null;
        
        System.out.println("Cutting signals file...");
        try {
            
            writer = new BufferedWriter(new FileWriter("C:\\Users\\danilo.bernardi\\Documents\\VRPhysioPlatform\\20180511_171124 (Jacky)\\signals.txt"));
            
            columns[0] = "seqNumber";
            columns[1] = "digitalInput";
            
            long newTimestamp = timestamp.getTime() - (long)(start*1000);
            SignalsConfiguration signalsConfiguration = new SignalsConfiguration(deviceMAC, SAMPLING_RATE, columns, newTimestamp);
            
            Gson gson = new Gson();
            String jsonSignals = gson.toJson(signalsConfiguration);

            writer.write("# " + jsonSignals + System.lineSeparator());
            
            br = new BufferedReader(new FileReader("C:\\Users\\danilo.bernardi\\Documents\\VRPhysioPlatform\\20180511_171124 (Jacky)\\signals_TEMP.txt"));
            
            // ignore first START signals
            for (int i = 0; i < start; i++){
                br.readLine();
            }
            
            // copy data until END
            for (int i = 0; i < end; i++){
                String line = br.readLine();
                String[] parts = line.split("\t");
                int[] sensorsData = new int[8];
                for (int j = 0; j < 8; j++){
                    sensorsData[j] = Integer.parseInt(parts[j+2]);
                }

                Device.Frame frame = new Device.Frame();
                frame.seq = (byte)i;
                frame.dig_in = Boolean.valueOf(parts[1]);
                frame.an_in = sensorsData;
                
                String output = i + "\t" + Boolean.valueOf(parts[1]) + "\t";
                
                int j;
                for (j = 0; j < 7; j++){
                    output += Integer.parseInt(parts[j+2]) + "\t";
                }
                output += Integer.parseInt(parts[j+2]);

                writer.write(output + System.lineSeparator());  
            }
            
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "AAAAAAAAAAA", ex);
        } finally {
            try {
                writer.close();
                br.close();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("...done");
    }
    
    private String readFile(String filePath){
        String result = null;
        try {
            File file = new File(filePath);
            if (file.exists()){
                FileInputStream fis = new FileInputStream(file);
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                fis.close();
                result = new String(data, "UTF-8");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    private double transTemp(int rawData){
        double ntcV = rawData*VCC/POW_2_N;
        double ntcO = 10000*ntcV/(VCC-ntcV);
        double logO = Math.log(ntcO);
        double tempK = 1/(A0+(A1*logO)+(A2*Math.pow(logO,3)));
        double tempC = tempK-KELVIN_CELSIUS;      
        return tempC;
    }
    
    private double transAcc(int rawData){
        double acc = (2*(rawData-C_MIN)/(C_MAX-C_MIN))-1;
        return acc;
    }
    
    private double transResp(int rawData){
        double perc = ((rawData/POW_2_N)-0.5)*100;
        return perc;
    }
    
    private double transEmg(int rawData){
        double emgV = (((rawData/POW_2_N)-0.5)*VCC)/EMG_ECG_GAIN;
        double emgMV = emgV*1000;
        return emgMV;
    }
    
    private double transEda(int rawData){
        double edaMS = (rawData/POW_2_N*VCC)/EDA_CONST;
        return edaMS;
    }
    
    private double transEcg(int rawData){
        double ecgV = (((rawData/POW_2_N)-0.5)*VCC)/EMG_ECG_GAIN;
        double ecgMV = ecgV*1000;
        return ecgMV;
    }
    
    private double transEeg(int rawData){
        double eegV = (((rawData/POW_2_N)-0.5)*VCC)/EEG_GAIN;
        double eegMV = eegV*1000000;
        return eegMV;
    }
    
    private double transForce(int rawData){
        double force = rawData*100/POW_2_N;
        return force;
    }
    
    private double round(double value, int places) {
        if (places < 0) 
            throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    protected void stopAcquisitions(){
        if (signalsTimer != null)
            signalsTimer.shutdown();
        
        if (cameraTimer != null)
            cameraTimer.shutdown();
        

        try {
            if (signalsWriter != null)
                signalsWriter.close();
            if (dev != null)
                dev.Close();
            if (grabber != null){
                grabber.stop();
                grabber.close();
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        
        // delete working directory if empty
        File f = new File(currentFolder);
        if(f.isDirectory() && f.exists()){
            String[] files = f.list();
            if (files.length <= 1){
                if (files.length == 1){
                    String path = currentFolder + "\\" + files[0];
                    boolean deleteResult = new File(path).delete();
                    if (!deleteResult){
                        logger.log(Level.SEVERE, "Deletion of temp signals file failed");
                    }
                }
                boolean deleteResult = f.delete();
                    if (!deleteResult){
                        logger.log(Level.SEVERE, "Deletion of working directory failed");
                    }
            }
        }
    }
    
    private void print(String s){
        System.out.println(s);
    }
    
    class CutVideo implements Runnable{

        private final String video;
        private final String videoPathIn;
        private final String videoPathOut;
        private final double start;
        private final double end;

        public CutVideo(String video, String videoPathIn, String videoPathOut, double start, double end) {
            this.video = video;
            this.videoPathIn = videoPathIn;
            this.videoPathOut = videoPathOut;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public void run() {
            System.out.println("CUTTING " + video + "...");
            progressBar.setVisible(true);
            syncLabel.setVisible(true);
            try {
                Process processDuration = new ProcessBuilder(
                        ffmpeg,
                        "-i",
                        videoPathIn,
                        "-ss",
                        String.valueOf(start),
                        "-t",
                        String.valueOf(end),
                        "-y",
                        videoPathOut)
                        .redirectErrorStream(true).start();

                StringBuilder strBuild = new StringBuilder();
                BufferedReader processOutputReader = new BufferedReader(new InputStreamReader(processDuration.getInputStream(), Charset.defaultCharset()));
                String line;
                while ((line = processOutputReader.readLine()) != null) {
                    strBuild.append(line).append(System.lineSeparator());
                }
                processDuration.waitFor();

                String outputJson = strBuild.toString().trim();
                System.out.println(outputJson);
                
                if (video.equals(CAMERA_VIDEO)){
                    cutCameraCompleted = true;
                }
                else {
                    cutVrCompleted = true;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }
    
    class CameraRunnable implements Runnable{

        @Override
        public void run() {
            
//            // Thread for audio capture, this could be in a nested private class if you prefer...
//            new Thread(new Runnable() {
//                @Override
//                public void run()
//                {
//                    // Pick a format...
//                    // NOTE: It is better to enumerate the formats that the system supports,
//                    // because getLine() can error out with any particular format...
//                    // For us: 44.1 sample rate, 16 bits, stereo, signed, little endian
//                    AudioFormat audioFormat = new AudioFormat(44100.0F, 16, 2, true, false);
//
//                    // Get TargetDataLine with that format
//                    Mixer.Info[] minfoSet = AudioSystem.getMixerInfo();
//                    int AUDIO_DEVICE_INDEX = 4;
//                    Mixer mixer = AudioSystem.getMixer(minfoSet[AUDIO_DEVICE_INDEX]);
//                    DataLine.Info dataLineInfo = new DataLine.Info(javax.sound.sampled.TargetDataLine.class, audioFormat);
//
//                    try
//                    {
//                        // Open and start capturing audio
//                        // It's possible to have more control over the chosen audio device with this line:
//                        // TargetDataLine line = (TargetDataLine)mixer.getLine(dataLineInfo);
//                        TargetDataLine line = (TargetDataLine)AudioSystem.getLine(dataLineInfo);
//                        line.open(audioFormat);
//                        line.start();
//
//                        int sampleRate = (int) audioFormat.getSampleRate();
//                        int numChannels = audioFormat.getChannels();
//
//                        // Let's initialize our audio buffer...
//                        int audioBufferSize = sampleRate * numChannels;
//                        byte[] audioBytes = new byte[audioBufferSize];
//
//                        // Using a ScheduledThreadPoolExecutor vs a while loop with
//                        // a Thread.sleep will allow
//                        // us to get around some OS specific timing issues, and keep
//                        // to a more precise
//                        // clock as the fixed rate accounts for garbage collection
//                        // time, etc
//                        // a similar approach could be used for the webcam capture
//                        // as well, if you wish
//                        ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);
//                        exec.scheduleAtFixedRate(new Runnable() {
//                            @Override
//                            public void run()
//                            {
//                                try
//                                {
//                                    // Read from the line... non-blocking
//                                    int nBytesRead = 0;
//                                    while (nBytesRead == 0) {
//                                        nBytesRead = line.read(audioBytes, 0, line.available());
//                                    }
//
//                                    // Since we specified 16 bits in the AudioFormat,
//                                    // we need to convert our read byte[] to short[]
//                                    // (see source from FFmpegFrameRecorder.recordSamples for AV_SAMPLE_FMT_S16)
//                                    // Let's initialize our short[] array
//                                    int nSamplesRead = nBytesRead / 2;
//                                    short[] samples = new short[nSamplesRead];
//
//                                    // Let's wrap our short[] into a ShortBuffer and
//                                    // pass it to recordSamples
//                                    ByteBuffer.wrap(audioBytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(samples);
//                                    ShortBuffer sBuff = ShortBuffer.wrap(samples, 0, nSamplesRead);
//
//                                    // recorder is instance of
//                                    // org.bytedeco.javacv.FFmpegFrameRecorder
//                                    recorder.recordSamples(sampleRate, numChannels, sBuff);
//                                } 
//                                catch (org.bytedeco.javacv.FrameRecorder.Exception e)
//                                {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }, 0, (long) 1000 / FRAME_RATE, TimeUnit.MILLISECONDS);
//                    } 
//                    catch (LineUnavailableException e1)
//                    {
//                        e1.printStackTrace();
//                    }
//                }
//            }).start();

            Frame capturedFrame = null;
            try {
                capturedFrame = grabber.grab();
            } catch (FrameGrabber.Exception ex) {
                logger.log(Level.SEVERE, null, ex);
            }

            Image image = SwingFXUtils.toFXImage(converter.convert(capturedFrame), null);  
            
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    cameraView.setImage(image);
                }
            });
            
            if (record){
                if (startTime == 0){
                    startTime = System.currentTimeMillis();
                    timestamp = new Timestamp(startTime);
                }
                // Create timestamp for this frame
                videoTS = 1000 * (System.currentTimeMillis() - startTime);

                // Check for AV drift
                if (videoTS > recorder.getTimestamp()){
//                    System.out.println(
//                            "Lip-flap correction: " 
//                            + videoTS + " : "
//                            + recorder.getTimestamp() + " -> "
//                            + (videoTS - recorder.getTimestamp()));

                    // We tell the recorder to write this frame at this timestamp
                    recorder.setTimestamp(videoTS);
                }

                try {
                    // Send the frame to the org.bytedeco.javacv.FFmpegFrameRecorder
                    recorder.record(capturedFrame);
                } catch (FrameRecorder.Exception ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }

        }
    }
    
}
