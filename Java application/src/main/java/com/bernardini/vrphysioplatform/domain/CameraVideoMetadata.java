
package com.bernardini.vrphysioplatform.domain;

/**
 *
 * @author danilo.bernardi
 */

public class CameraVideoMetadata {

    private long timestamp;
    private int frameRate;
    private long duration;
    private long size;
    private String path;

    public CameraVideoMetadata(long startTs, int frameRate, long duration, long size, String format, String path) {
        this.timestamp = startTs;
        this.frameRate = frameRate;
        this.duration = duration;
        this.size = size;
        this.path = path;
    }

    public long getStartTs() {
        return timestamp;
    }

    public void setStartTs(long startTs) {
        this.timestamp = startTs;
    }

    public int getFrameRate() {
        return frameRate;
    }

    public void setFrameRate(int frameRate) {
        this.frameRate = frameRate;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.timestamp ^ (this.timestamp >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CameraVideoMetadata other = (CameraVideoMetadata) obj;
        if (this.timestamp != other.timestamp) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CameraVideoMetadata{" + "startTs=" + timestamp + ", frameRate=" + frameRate + ", duration=" + duration + ", size=" + size + ", path=" + path + '}';
    }
    
    
    
}
