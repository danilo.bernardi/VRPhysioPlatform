
package com.bernardini.vrphysioplatform.domain;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Marker extends Rectangle{

    private long timestamp;
    private String name;
    private double offset;

    public Marker(long timestamp, double width, double height, double offset) {
        super(width,height);
        this.offset = offset;
        this.timestamp = timestamp;
    }
    
    public Marker(String name, long timestamp, double width, double height, double offset) {
        super(width,height);
        this.name = name;
        this.offset = offset;
        this.timestamp = timestamp;
    }

    public double getOffset(){
        return offset;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
}
