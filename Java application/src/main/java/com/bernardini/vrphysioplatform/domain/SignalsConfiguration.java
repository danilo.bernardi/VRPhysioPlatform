
package com.bernardini.vrphysioplatform.domain;

import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author danilo.bernardi
 */

public class SignalsConfiguration {

    private String deviceId;
    private int samplingRate;
    private String[] columns;
    private long timestamp;

    public SignalsConfiguration(String deviceId, int samplingRate, String[] columns, long timestamp) {
        this.deviceId = deviceId;
        this.samplingRate = samplingRate;
        this.columns = columns;
        this.timestamp = timestamp;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    
    public int getSamplingRate() {
        return samplingRate;
    }

    public void setSamplingRate(int samplingRate) {
        this.samplingRate = samplingRate;
    }

    public String[] getColumns() {
        return columns;
    }

    public void setColumns(String[] columns) {
        this.columns = columns;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.deviceId);
        hash = 71 * hash + this.samplingRate;
        hash = 71 * hash + Arrays.deepHashCode(this.columns);
        hash = 71 * hash + (int) (this.timestamp ^ (this.timestamp >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SignalsConfiguration other = (SignalsConfiguration) obj;
        if (this.samplingRate != other.samplingRate) {
            return false;
        }
        if (this.timestamp != other.timestamp) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Arrays.deepEquals(this.columns, other.columns)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SignalsConfiguration{" + "deviceId=" + deviceId + ", samplingRate=" + 
                samplingRate + ", columns=" + columns + ", timestamp=" + timestamp + '}';
    }

}
