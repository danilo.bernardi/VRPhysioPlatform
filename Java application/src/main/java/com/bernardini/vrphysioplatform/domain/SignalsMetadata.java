
package com.bernardini.vrphysioplatform.domain;

/**
 *
 * @author danilo.bernardi
 */

public class SignalsMetadata {
    
    private long timestamp;
    private int samplingRate;
    private long duration;
    private long samples;
    private String path;

    public SignalsMetadata(long timestamp, int samplingRate, long duration, long samples, String path) {
        this.timestamp = timestamp;
        this.samplingRate = samplingRate;
        this.duration = duration;
        this.samples = samples;
        this.path = path;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getSamplingRate() {
        return samplingRate;
    }

    public void setSamplingRate(int samplingRate) {
        this.samplingRate = samplingRate;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getSamples() {
        return samples;
    }

    public void setSamples(long samples) {
        this.samples = samples;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "SignalsMetadata{" + "timestamp=" + timestamp + ", samplingRate=" + samplingRate + ", duration=" + duration + ", samples=" + samples + ", path=" + path + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (int) (this.timestamp ^ (this.timestamp >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SignalsMetadata other = (SignalsMetadata) obj;
        if (this.timestamp != other.timestamp) {
            return false;
        }
        return true;
    }
    
    
}
