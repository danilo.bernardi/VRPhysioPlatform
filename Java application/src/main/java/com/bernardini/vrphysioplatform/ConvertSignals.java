
package com.bernardini.vrphysioplatform;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author danilo.bernardi
 */

public class ConvertSignals {
    
    private static final int SAMPLING_RATE = 1000;
    private static final int FRAME_RATE = 30;
    private static final int GOP_LENGTH_IN_FRAMES = 60;
    private static final int N_BITS = 16;
    private static final double A0 = 1.12764514e-3;
    private static final double A1 = 2.34282709e-4;
    private static final double A2 = 8.77303013e-8;
    private static final int VCC = 3;
    private static final double KELVIN_CELSIUS = 273.15;
    private static final double C_MIN = 27400;
    private static final double C_MAX = 39100;
    private static final double EMG_ECG_GAIN = 1000;
    private static final double EEG_GAIN = 40000;
    private static final double EDA_CONST = 0.12;
    private static final double POW_2_N = Math.pow(2, N_BITS);
    
    public static void main(String[] args){
        String folderPath = "C:\\Users\\danilo.bernardi\\Documents\\VRPhysioPlatform\\20180514_131511";
        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(folderPath + "\\signalsConv.txt"));
            br = new BufferedReader(new FileReader(folderPath + "\\signals.txt"));
            
            String line = br.readLine().substring(2);
            Gson gson = new Gson();
            JsonObject properties = gson.fromJson(line, JsonObject.class);
            JsonArray columnsArray = properties.get("columns").getAsJsonArray();
            String columns = columnsArray.get(0) + "\t" + 
                    columnsArray.get(1) + "\t" + 
                    columnsArray.get(2) + "\t" + 
                    columnsArray.get(3) + "\t" + 
                    columnsArray.get(4) + "\t" + 
                    columnsArray.get(5) + "\t" + 
                    columnsArray.get(6) + "\t" + 
                    columnsArray.get(7) + "\t" + 
                    columnsArray.get(8);
            bw.write(columns);
            bw.newLine();
            
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                String newLine = "";
                String[] parts = line.split("\t");
                
                double seqNum = Double.parseDouble(parts[0]);
                double sec = seqNum/1000;
                
                newLine += sec + "\t";
                int digIn = 0;
                if (Boolean.valueOf(parts[1]))
                    digIn = 1;
                newLine += digIn + "\t";
                
                int accX = Integer.parseInt(parts[2]);
                int accY = Integer.parseInt(parts[3]);
                int accZ = Integer.parseInt(parts[4]);
                int resp = Integer.parseInt(parts[5]);
                int eda = Integer.parseInt(parts[6]);
                int ecg = Integer.parseInt(parts[7]);
                int emg = Integer.parseInt(parts[8]);
                
                double transAccX = round(transAcc(accX),3);
                double transAccY = round(transAcc(accY),3);
                double transAccZ = round(transAcc(accZ),3);
                double transResp = round(transResp(resp),3);
                double transEda = round(transEda(eda),3);
                double transEcg = round(transEcg(ecg),3);
                double transEmg = round(transEmg(emg),3);
                
                newLine += transAccX + "\t" +
                        transAccY + "\t" +
                        transAccZ + "\t" +
                        transResp + "\t" +
                        transEda + "\t" +
                        transEcg + "\t" +
                        transEmg;
                
                bw.write(newLine);
                bw.newLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConvertSignals.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ConvertSignals.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
                bw.close();
            } catch (IOException ex) {
                Logger.getLogger(ConvertSignals.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static double round(double value, int places) {
        if (places < 0) 
            throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    private static double transAcc(int rawData){
        double acc = (2*(rawData-C_MIN)/(C_MAX-C_MIN))-1;
        return acc;
    }
    
    private static double transResp(int rawData){
        double perc = ((rawData/POW_2_N)-0.5)*100;
        return perc;
    }
    
    private static double transEmg(int rawData){
        double emgV = (((rawData/POW_2_N)-0.5)*VCC)/EMG_ECG_GAIN;
        double emgMV = emgV*1000;
        return emgMV;
    }
    
    private static double transEda(int rawData){
        double edaMS = (rawData/POW_2_N*VCC)/EDA_CONST;
        return edaMS;
    }
    
    private static double transEcg(int rawData){
        double ecgV = (((rawData/POW_2_N)-0.5)*VCC)/EMG_ECG_GAIN;
        double ecgMV = ecgV*1000;
        return ecgMV;
    }
    
}
